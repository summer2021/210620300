<?php


$lang->moduleOrder[60] = 'okr';

$lang->resource->okr = new stdclass();
$lang->resource->okr->index   = 'index';
$lang->resource->okr->add  = 'add';
$lang->resource->okr->edit    = 'edit';
$lang->resource->okr->save    = 'save';
$lang->resource->okr->delete  = 'delete';
$lang->resource->okr->period  = 'period';

$lang->resource->okr->createPeriod  = 'createPeriod';
$lang->resource->okr->deletePeriod  = 'deletePeriod';
$lang->resource->okr->setDefaultPeriod  = 'setDefaultPeriod';

$lang->okr->methodOrder[5]  = 'index';
$lang->okr->methodOrder[10] = 'add';
$lang->okr->methodOrder[15] = 'edit';
$lang->okr->methodOrder[25] = 'save';
$lang->okr->methodOrder[30] = 'delete';
$lang->okr->methodOrder[40] = 'period';
$lang->okr->methodOrder[44] = 'createPeriod';
$lang->okr->methodOrder[45] = 'deletePeriod';
$lang->okr->methodOrder[47] = 'setDefaultPeriod';
