<?php

/**
 * The control file of doc module of ZenTaoPMS.
 *
 * @copyright   Copyright 2009-2015 青岛易软天创网络科技有限公司(QingDao Nature Easy Soft Network Technology Co,LTD, www.cnezsoft.com)
 * @license     ZPL (http://zpl.pub/page/zplv12.html)
 * @author      Chunsheng Wang <chunsheng@cnezsoft.com>
 * @package     okr
 * @version     $Id: control.php 933 2010-07-06 06:53:40Z wwccss $
 * @link        http://www.zentao.net
 */
class okr extends control
{
    /**
     * Construct function, load user, tree, action auto.
     *
     * @access public
     * @return void
     */
    public function __construct($moduleName = '', $methodName = '')
    {
        parent::__construct($moduleName, $methodName);
//        $this->loadModel('user');
    }

    public function json($data, $msg = "请求成功", $code = 200)
    {
        header('Content-Type:application/json; charset=utf-8');
        $ret['data'] = $data;
        $ret['msg']  = $msg;
        $ret['code'] = $code;
        exit(json_encode($ret));
    }

    /**
     * index page.
     *
     * @access public
     * @return void
     */
    public function index()
    {
        $periods = $this->okr->getAllPeriod();
        if (empty($periods)) {
           $this->redirect(helper::createLink('okr','empty'));
        }
        $okrs = $this->okr->getOKR();
        if (empty($okrs)) {
            $this->view->isEmpty = true;
        }
        $this->view->okrs = $okrs;
        $this->display();
    }

    public  function redirect($url)
    {
        header("Location: $url");
        exit();
    }
    public function empty()
    {
        $okrs = $this->okr->getOKR();
        if (empty($okrs)) {
            $this->view->isEmpty = true;
        }
        $this->view->okrs = $okrs;
        $this->display();
    }

    public function period($orderBy = 'id_desc', $recTotal = 0, $recPerPage = 20, $pageID = 1)
    {
        $this->app->loadClass('pager', $static = true);
        $pager                  = new pager($recTotal, $recPerPage, $pageID);
        $this->view->title      = "okr";
        $this->view->periodList = $this->okr->getPeriodList($orderBy, $pager);
        $this->view->orderBy    = $orderBy;
        $this->view->pager      = $pager;
        $this->display();
    }

    public function createPeriod()
    {
        if ($_POST) {
            $id = $this->okr->createPeriod();
            if (dao::isError()) $this->send(array('result' => 'fail', 'message' => dao::getError()));
//            if ($this->viewType == 'json') {
//                $this->send([
//                    'result'  => 'success',
//                    'message' => $this->lang->saveSuccess,
//                    'id'      => $id
//                ]);
//            }
            $this->send(array('result' => 'success', 'message' => $this->lang->saveSuccess, 'locate' => inlink('period')));
        }
        $this->view->title      = $this->lang->okr->create;
        $this->view->position[] = html::a(inlink('period'), $this->lang->okr->common);
        $this->view->position[] = $this->lang->okr->create;
        $this->display();
    }

    public function deletePeriod($okrId, $confim = 'no')
    {
        if ($confim != 'yes') die(js::confirm("确定要删除吗?", inlink('deletePeriod', "okrId=$okrId&confirm=yes")));
        $this->okr->delete('zt_okr_period', $okrId);
        $this->dao->update('zt_okr_objective')
            ->set('deleted')->eq(1)
            ->where('period')->eq($okrId)
            ->andWhere('owner')->eq($this->app->user->account)
            ->exec();
        die(js::reload('parent'));
    }

    public function setDefaultPeriod($id)
    {
        $this->setDefaultPeriodById($id);
        die(js::reload('parent'));
    }


    /*************************************************ajax相相关***********************************/
    public function ajax_get_period()
    {
        $periods = $this->okr->getAllPeriod();
        $has     = false;
        foreach ($periods as $k => $v) {
            $periods[$k]->is_select = (bool)($v->isDefault);
            if ($v->isDefault) {
                $has = true;
            }
        }
        if ($has === false) {
            $periods[0]->is_select = true;
        }
        $this->json($periods);
    }

    /***
     * 获取周期下的 objective
     */
    public function ajax_get_period_data()
    {
        $data   = $this->initPost();
        $this->setDefaultPeriodById($data['id']);
        $period = $this->okr->getOkrByPeriodId($data['id']);
        $this->json($period);
    }

    /***
     * 修改 objective
     */
    public function ajax_upsert_objective()
    {
        $data = $this->initPost();
        if (!empty($data)) {
            if ($data['id'] > 0) {
                $this->dao->update('zt_okr_objective')
                    ->set('content')->eq($data['content'])
                    ->set('period')->eq($data['period'])
                    ->set('weight')->eq($data['weight']*100)
                    ->set('progress')->eq($data['progress']*100)
                    ->set('score')->eq($data['score']*100)
                    ->set('owner')->eq($this->app->user->account)
//                    ->set('createdDate')->eq(date('Y-m-d H:i:s'))
                    ->set('lastEditedDate')->eq(date('Y-m-d H:i:s'))
                    ->where('id')->eq($data['id'])
                    ->exec();
                //delete old krs
                $this->dao->delete()->from('zt_okr_key_result')->where('objective')->eq($data['id'])->exec();
                foreach ($data['kr'] as $key => $kr) {
                    $this->dao->insert('zt_okr_key_result')
                        ->set('objective')->eq($data['id'])
                        ->set('content')->eq($kr['content'])
                        ->set('weight')->eq($kr['weight']*100)
                        ->set('progress')->eq($kr['progress']*100)
                        ->set('score')->eq($kr['score']*100)
                        ->set('sort')->eq($kr['sort'])
                        ->set('createdDate')->eq(date('Y-m-d H:i:s'))
                        ->set('lastEditedDate')->eq(date('Y-m-d H:i:s'))
                        ->exec();
                }
            } else {
                $this->dao->insert('zt_okr_objective')
                    ->set('content')->eq($data['content'])
                    ->set('period')->eq($data['period'])
                    ->set('weight')->eq($data['weight']*100)
                    ->set('progress')->eq($data['progress']*100)
                    ->set('score')->eq($data['score']*100)
                    ->set('owner')->eq($this->app->user->account)
                    ->set('createdDate')->eq(date('Y-m-d H:i:s'))
                    ->set('lastEditedDate')->eq(date('Y-m-d H:i:s'))
                    ->exec();
                $id=$this->dao->lastInsertID();
                foreach ($data['kr'] as $key => $kr) {
                    $this->dao->insert('zt_okr_key_result')
                        ->set('objective')->eq($id)
                        ->set('content')->eq($kr['content'])
                        ->set('weight')->eq($kr['weight']*100)
                        ->set('progress')->eq($kr['progress']*100)
                        ->set('score')->eq($kr['score']*100)
                        ->set('sort')->eq($kr['sort'])
                        ->set('createdDate')->eq(date('Y-m-d H:i:s'))
                        ->set('lastEditedDate')->eq(date('Y-m-d H:i:s'))
                        ->exec();
                }
            }
        }
        $this->json([]);
    }


    public function ajax_delete_objective()
    {
        $data        = $this->initPost();
        if(!empty($data['objective_id'])){
            $objectiveId = $data['objective_id'];
            $this->dao->update('zt_okr_objective')
                ->set('deleted')->eq(1)
                ->where('id')->eq($objectiveId)
                ->andWhere('owner')->eq($this->app->user->account)
                ->exec();
            $this->dao->update('zt_okr_key_result')
                ->set('deleted')->eq(1)
                ->where('objective')->eq($objectiveId)
                ->exec();
            $this->json([]);
        }
        $msg = "error id";
        $code = 500;
        $this->json([],$msg,$code);
    }

    public function ajax_alignment_objective()
    {
        $data = $this->initPost();
        if ($data) {
            $this->dao->update('zt_okr_objective')
                ->set('alignment')->eq($data['alignment_objective_id'])
                ->where('id')->eq($data['objective_id'])
                ->andWhere('owner')->eq($this->app->user->account)
                ->exec();
            $this->json([]);
        }
        $this->loadModel('user');
        $users  = $this->user->getPairs('noletter|noclosed|nodeleted|noempty');
        $select = [];
        foreach ($users as $k => $user) {
            if (!empty($user)) {
                $count = $this->dao->select('*')->from('zt_okr_objective')
                    ->where('owner')->eq($k)
                    ->andWhere('deleted')->eq(0)
                    ->fetchAll();
                if (!empty($count)) {
                    $temp['contData'] = $count;
                    $temp['admin'] = $user;
                    $select[]=$temp;
                }
            }
        }
        $this->json($select);
    }

    public function initPost()
    {
        if (empty($_POST) && false !== strpos($_SERVER["CONTENT_TYPE"], 'application/json')) {
            $content = file_get_contents('php://input');
            $post    = (array)json_decode($content, true);
        } else {
            $post = $_POST;
        }
        return $post;
    }

    /**
     * @param $id
     */
    private function setDefaultPeriodById($id)
    {
        $this->dao->update('zt_okr_period')
            ->set('isDefault')->eq(1)
            ->where('id')->eq($id)
            ->andWhere('createdBy')->eq($this->app->user->account)
            ->exec();
        $this->dao->update('zt_okr_period')
            ->set('isDefault')->eq(0)
            ->where('id')->ne($id)
            ->andWhere('createdBy')->eq($this->app->user->account)
            ->exec();
    }

}
