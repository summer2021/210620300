// 1.进入页面获取日期列表
getDateList()

// 2.获取所有的.add-key .add标签
let addKeyList;
// 获取所有的.objective-item标签
let addKeyParentList;

// 点击添加新的key
$(document).on("click", '.add-key .add', function (el) {
  // 还没点击修改okr前，不允许添加新的key
  if ($($(el.target).parents('.objective-item')[0]).hasClass('allow-change-info')) return

  // 拿到当前点击的add-key对应的下标
  let parentIdx = $(addKeyList).index(this);
  // 拿到点击add-key对应的父元素
  const currentParent = addKeyParentList[parentIdx];
  // 找到该父元素中的ul标签
  const currentUl = $(currentParent).find('.enter-key ul')[0];
  // 拿到当前ul标签的子元素个数
  const idx = $(currentUl).children().length;
  let htm =
      `<li>
      <p class="left">
        <span>KR${idx}</span>
        <input class="keyInput" type="text" placeholder="请输入Key Result的值">
      </p>
      <p class="right">
        <input class="progress-val" value="0%"></input>
        <input class="scores" value="0"></input>
        <input class="weights" value="0%"></input>
        <span class="delete">×</span>
      </p>
    </li > 
    `
  $(currentUl).append(htm);
  // 移除发布按钮的状态
  currentBottomBtn && $(currentBottomBtn.children[0]).removeClass('allow-submission');
  // 给以每一项权重赋值
  calculateWeights(el, 'add-key')
})

// 3.点击添加新的Objective
$('.add-objective').click(function () {
  const idx = $('#objective-box').children().length
  let htm =
      `<div class="objective-item">
      <div class="top-content">
        <div class="serial-number">
          <span>0${idx + 1}</span>
        </div>
        <div class="content">
          <div class="add-align">
            <p class="left">
            </p>
            <p class="right">
              <span>进度</span>
              <span>分数</span>
              <span>权重</span>
            </p>
          </div>
          <div class="enter-key">
            <ul>
              <li>
                <p class="left">
                  <input class="keyInput" type="text" placeholder="添加objective名字">
                </p>
                <p class="right">
                  <span class="default progress-value">0%</span>
                  <span class="default scores-value">0</span>
                  <span class="default">100%</span>
                  <span class="default-delete">×</span>
                </p>
              </li>
            </ul>
          </div>
          <div class="add-key">
            <p class="add">
              <span>+</span>
              <span>添加Key&nbsp;Result</span>
            </p>
          </div>
        </div>
      </div>
    <div class="bottom-btn">
      <span class="confirm-submit">发布</span>
      <span class="cancel-submit">取消</span>
    </div>
  </div>
  `
  $('#objective-box').append(htm);
  // 获取最新所有的.add-key .add标签
  addKeyList = document.querySelectorAll('.add-key .add');
  // 获取最新所有的.objective-item标签
  addKeyParentList = document.querySelectorAll('.objective-item');
})

// 4.input值有没有输入，或者权重总和没有100%的不允许点击发布
let currentBottomBtn;
$(document).on("blur", 'input', function (el) {
  // 每次进入后权重总和置为0
  let totolWeights = 0;
  // 每次进入后进度总和置为0
  let totolProgress = 0;
  // 每次进入后分数总和置为0
  let totolScores = 0;
  // 如果是日期的input，则暂时阻止执行
  if (el.target.className == 'dataInput') return;

  // 拿到当前Input元素所在的ul
  const currentUl = $(el.target).parents('ul')[0];
  const inputList = $(currentUl).find('input');
  const curWeightsInput = $(currentUl).find('input.weights');
  const curProgressInput = $(currentUl).find('input.progress-val');
  const curScoresInput = $(currentUl).find('input.scores');
  // 判断是否每个input都有值
  let targetInput = true
  $(inputList).each((index, el) => {
    if ($.trim($(el).val()) == '') {
      targetInput = false
    }
  });

  // 权重、进度和分数有填的不对的地方就改为false
  let progressScores = true;
  // 权重Input失去焦点时才进行判断
  // if (el.target.className == 'weights') {
  $(curWeightsInput).each((index, item) => {
    if ($(item).val().indexOf('%') == -1 || isNaN(parseInt($(item).val()))) {
      progressScores = false;
      remindInfoBox('权重填写有误 ！请填写0% ~ 100%的值');
      $(item).val('0%');
      return
    } else {
      progressScores = true;
      totolWeights += Number($(item).val().slice(0, -1));
    }
  })
  // }

  if (totolWeights > 100) {
    return remindInfoBox('Key Result的权重总和不能大于100% !');
  }

  // 进度Input失去焦点时才进行判断
  if (el.target.className == 'progress-val') {
    $(curProgressInput).each((index, item) => {
      if ($(item).val().indexOf('%') == -1 || isNaN(parseInt($(item).val())) || parseInt($(item).val()) > 100) {
        remindInfoBox('进度填写有误 ！请填写0% ~ 100%的值');
        progressScores = false;
        $(item).val('0%');
        return;
      } else {
        progressScores = true;
      }
    })
  }
  // 分数Input失去焦点时才进行判断
  if (el.target.className == 'scores') {
    $(curScoresInput).each((index, item) => {
      if (isNaN(parseInt($(item).val())) || Number($(item).val()) > 1) {
        remindInfoBox('分数填写有误 ！请填写0 ~ 1的值');
        $(item).val('0');
        progressScores = false;
        return;
      } else {
        progressScores = true;
      }
    })
  }

  if (el.target.className == 'weights' || el.target.className == 'progress-val' || el.target.className == 'scores') {
    // 计算出总进度的值和总分数
    $(curProgressInput).each((index, item) => {
      totolProgress += Number($(item).val().slice(0, -1)) * Number($(curWeightsInput[index]).val().slice(0, -1)) / 100;
      totolScores += Number($(curScoresInput[index]).val()) * Number($(curWeightsInput[index]).val().slice(0, -1)) / 100;
    })
    totolProgress = totolProgress == 0 ? totolProgress + '%' : totolProgress.toFixed(1) + '%';
    totolScores = totolScores == 0 ? totolScores : totolScores.toFixed(2);
    // 将总进度的值和总分数显示到页面上
    $($($($(el.target).parents('ul')[0])).find('.right .progress-value')[0]).html(totolProgress);
    $($($($(el.target).parents('ul')[0])).find('.right .scores-value')[0]).html(totolScores);
  }

  // 拿到对应的发布按钮
  currentBottomBtn = $(el.target).parents('.objective-item')[0].children[1];
  // 如果全部填写了值，且总权重小于或等于100，则添加样式，否则去掉
  if (targetInput && totolWeights <= 100 && totolWeights > 99.99999999999 && progressScores) {
    $(currentBottomBtn.children[0]).addClass('allow-submission');
  } else {
    $(currentBottomBtn.children[0]).removeClass('allow-submission');
  }
})

// 5.点击删除当前Objective，先给弹框，点击确认后再删除
let curretnDeleteObjective; // 当前要删除的Objective对象
let cancelSubmitCode = false // 点击取消触发的状态
// 点×
$(document).on("click", '.default-delete', function (el) {
  $('.remind-dialog-box .content').html('您确认删除当前Objective吗？');
  $('.remind-dialog-box').css('display', 'block');
  // 拿到当前Objective对象
  curretnDeleteObjective = $(el.target).parents('.objective-item')[0];
})
// 点取消
$(document).on("click", '.cancel-submit', function (el) {
  $('.remind-dialog-box .content').html('您确认取消编辑当前Objective吗？');
  $('.remind-dialog-box').css('display', 'block');
  // 拿到当前Objective对象
  curretnDeleteObjective = $(el.target).parents('.objective-item')[0];
  // 表明是点击的取消按钮
  cancelSubmitCode = true;
})

// 点击删除当前Objective中的某个key
$(document).on("click", '.delete', function (el) {
  // 在删除子元素前拿到对应的父元素，方便重新排序
  const targetUL = $($(el.target).parents('ul'))[0];

  // 拿到当前Input元素所在的ul
  const currentTarget = $(el.target).parents('li')[0];

  // 重新计算权重
  // calculateWeights(el, 'delete-key');
  // 然后移除当前key
  $(currentTarget).remove();

  // 重新排序
  const keysList = $(targetUL).find('.left span');
  $(keysList).each((index, item) => {
    $(item).html('KR' + (index + 1));
  })
})

// 6.点击发布按钮给出提示
$(document).on("click", '.confirm-submit', function (el) {
  // 拿到当前objective对象
  const objectiveItem = $(el.target).parents('.objective-item')[0];
  // 判断是否添加了Key Result
  // console.log($(objectiveItem).find('ul li').length)
  if ($(objectiveItem).find('ul li').length == 1) {
    return remindInfoBox('请至少添加一个Key Result ~ !', 1500);
  }

  // 拿到输入key的Input
  const keyInput = $(objectiveItem).find('input.keyInput');
  // 拿到权重input
  const weightsInput = $(objectiveItem).find('input.weights');
  // 拿到进度input
  const progressInput = $(objectiveItem).find('input.progress-val');
  // 拿到分数input
  const scoresInput = $(objectiveItem).find('input.scores');

  // 判断有没有没填的值
  let valueCode = false
  $(keyInput).each((index, el) => {
    if ($.trim($(el).val()) == '') {
      valueCode = true
    }
  })
  if (valueCode) {
    remindInfoBox('Objective名字或者Key Result未填 ~ !');
    return
  }

  let weightsTotal = 0;
  let weightsCode = false;
  // 权重部分
  $(weightsInput).each((index, item) => {
    if ($(item).val().indexOf('%') == -1 || isNaN(parseInt($(item).val()))) {
      weightsCode = true
    } else {
      weightsTotal += Number($(item).val().slice(0, -1))
    }
  })

  if (weightsCode) {
    remindInfoBox('权重填写有误 ！请填写0% ~ 100%的值');
    return
  }

  if (weightsTotal > 100 || weightsTotal < 99.99999999999) {
    remindInfoBox('权重总和必须为100% ~ !')
    return
  }

  // 全部验证成功以后，拿到对应的参数，然后请求接口
  let kr = [];
  $(weightsInput).each((index, item) => {
    let krItem = {
      'content': $(keyInput[index + 1]).val(), //儿子1的标题
      'weight': Number($(item).val().slice(0, -1)), //儿子1的权重
      'progress': Number($(progressInput[index]).val().slice(0, -1)), //儿子1的进度
      'score': Number($(scoresInput[index]).val()), //儿子1的分数
      'sort': index + 1, //儿子1是第几个
    };
    kr.push(krItem);
  })

  let params;
  params = {
    'content': $($(objectiveItem).find('.enter-key input.keyInput')[0]).val(),
    'period': currentDateId.id,//所属周期id
    'weight': 100,//父亲权重
    'progress': Number($($(objectiveItem).find('.right .progress-value')[0]).html().slice(0, -1)), //父亲进度
    'score': Number($($(objectiveItem).find('.right .scores-value')[0]).html()), //父亲分数
    'id': $(objectiveItem).find('.current-item-id').length == 0 ? 0 : $($(objectiveItem).find('.current-item-id')).html(), //父亲的id，添加为0，修改为对应的id，列表有返回
    'kr': kr
  }
  // console.log(params)
  addOrUpdateObjective(params)
})

// 7.计算权重的方法
function calculateWeights(val, type) {
  // 拿到所有的权重input
  const currentContent = $(val.target).parents('.content')[0];
  const weightsInput = $(currentContent).find('input.weights');

  let weightsValue, lastWeightsValue, middleValue;
  if (type == 'add-key' && weightsInput.length > 0) {
    if (weightsInput.length < 2) {
      weightsValue = (100 / weightsInput.length + '').slice(0, 4) + '%';
    } else {
      middleValue = (100 / weightsInput.length + '').slice(0, 4);
      lastWeightsValue = (100 - Number(middleValue) * (weightsInput.length - 1) + '').slice(0, 4) + '%';
      weightsValue = middleValue + '%';
    }
  } else if (type == 'delete-key' && weightsInput.length > 0) {
    if (weightsInput.length < 2) {
      weightsValue = (100 / weightsInput.length + '').slice(0, 4) + '%';
    } else {
      middleValue = (100 / weightsInput.length + '').slice(0, 4);
      lastWeightsValue = (100 - Number(middleValue) * (weightsInput.length - 1) + '').slice(0, 4) + '%';
      weightsValue = middleValue + '%';
    }
  }

  // 将权重赋值给每一项
  $(weightsInput).each((index, el) => {
    $(el).val(weightsValue)
  })
  // 最后一项需要特殊赋值
  if (weightsInput.length > 2) {
    $(weightsInput[weightsInput.length - 1]).val(lastWeightsValue)
  }
}

// 8.提示框的点击取消
$('.remind-cancel').click(function () {
  $('.remind-dialog-box').css('display', 'none');
  curretnDeleteObjective = null;
})
// 提示框的点击确认
$('.remind-confirm').click(function () {
  const curretnDeleteObjectiveId = $(curretnDeleteObjective).find('.current-item-id');
  // 临时添加的可以直接删除
  if (curretnDeleteObjectiveId.length == 0) {
    $(curretnDeleteObjective).remove();
    $('.remind-dialog-box').css('display', 'none');
  } else {
    if (cancelSubmitCode) {
      // 拿到该处原有的数据
      const primaryObject = dataList.find(el => el.id == $(curretnDeleteObjectiveId[0]).html());
      const primaryObjectIndex = dataList.findIndex(el => el.id == $(curretnDeleteObjectiveId[0]).html());
      // 清空现在的html内容
      $(curretnDeleteObjective).html('');
      // 重新定义新的内容
      let htm = '', htmr = '';
      htm =
          `
        <div class="top-content">
          <div class="serial-number">
            <span>0${primaryObjectIndex + 1}</span>
          </div>
          <div class="content">
            <div class="add-align">
              <div class="left">
                <span class="open-dialog-box">+</span>
                <span class="open-dialog-box">添加对齐</span>
                <span class="alignment-content">
                  <span class="cont-style">${primaryObject.alignment_owner}：</span>
                  <span class="cont-mes">${primaryObject.alignment_content}</span>
                </span>
                <div class="show-alignment-content-box"></div>
              </div>
              <div class="right">
                <span>进度</span>
                <span>分数</span>
                <span>权重</span>
              </div>
            </div>
            <div class="enter-key">
              <ul>
                <li>
                  <p class="left">
                    <input disabled="true" class="keyInput" type="text" placeholder="添加objective名字" value="${primaryObject.content}">
                  </p>
                  <p class="right">
                    <span class="default progress-value">${primaryObject.progress}%</span>
                    <span class="default scores-value">${primaryObject.score}</span>
                    <span class="default">${primaryObject.weight}%</span>
                    <span class="default-delete">×</span>
                  </p>
                </li>
              </ul>
            </div>
            <div class="add-key">
              <p class="add">
                <span>+</span>
                <span>添加Key&nbsp;Result</span>
              </p>
              <p class="update">
                 <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADQAAAAyCAIAAACcQ2+hAAAMZWlDQ1BJQ0MgUHJvZmlsZQAASImVlwdYU8kWgOeWVBJaIAJSQm+idAJICaFFEJAqiEpIAgklxoQgYkeXVXDtIoplRVdFXHR1BWQtiLjWRbG7lkURFWVdLNhQeRMS0HVf+d5839z5c+bMmXNOZu6dAUCngy+T5aG6AORLC+TxESGsialpLFIXwAAZGAJ3MIYvUMg4cXHRAJah9u/l9TWAqNrLLipb/+z/r0VfKFIIAEDSIWcKFYJ8yM0A4CUCmbwAAGIolFvPKJCpWAzZQA4dhDxbxdlqXqHiTDVvH9RJjOdCbgSATOPz5dkAaLdCOatQkA3taD+A7CoVSqQA6BhADhSI+ULIiZBH5edPU/F8yA5QXwZ5F2R25hc2s/9mP3PYPp+fPczquAYLOVSikOXxZ/6fqfnfJT9POTSHHaw0sTwyXhU/zOGN3GlRKqZB7pFmxsSqcg35rUSozjsAKFWsjExS66OmAgUX5g8wIbsK+aFRkE0hh0vzYqI18swsSTgPMlwtaJGkgJeoGbtYpAhL0NjcKJ8WHzvEWXIuRzO2ji8fnFel36rMTeJo7N8Qi3hD9l8VixNTIFMBwKiFkuQYyNqQDRS5CVFqHcyqWMyNGdKRK+NV/ttAZoukESFq+1h6ljw8XqMvy1cMxYuViiW8GA1XFogTI9X5wXYL+IP+G0GuF0k5SUN2RIqJ0UOxCEWhYerYsTaRNEkTL3ZXVhASrxnbK8uL0+jjZFFehEpuBdlEUZigGYuPLYCLU20fj5YVxCWq/cQzcvjj4tT+4IUgGnBBKGABJayZYBrIAZK2noYe+EvdEw74QA6ygQi4aCRDI1IGe6TwmQCKwZ+QREAxPC5ksFcECqH847BU/XQBWYO9hYMjcsFDyPkgCuTB38rBUdLh2ZLBAyiR/GN2AfQ1D1ZV3z9lHCiJ1kiUQ3ZZOkOaxDBiKDGSGE50xE3wQNwfj4bPYFjdcTbuO+TtZ33CQ0I74T7hKqGDcHOqpET+lS/jQQe0H66JOPPLiHE7aNMLD8EDoHVoGWfiJsAF94TzcPAgOLMXlHI1fqtiZ/2bOIcj+CLnGj2KKwWljKAEUxy+HqntpO01bEWV0S/zo/Y1czir3OGer+fnfpFnIWyjvtbEFmMHsFPYcewMdhhrACzsGNaInceOqHh4DT0YXENDs8UP+pML7Uj+MR9fM6cqkwrXWtdu1w+aPlAgKipQbTDuNNlMuSRbXMDiwK+AiMWTCkaPYrm7ursBoPqmqF9TL5mD3wqEefazrOQ7AAI8BwYGDn+WResA8DPcG9TOzzIHP/g6KALg9DKBUl6oluGqBwG+DXTgjjIG5sAaOMCI3IE38AfBIAyMA7EgEaSCKTDPYrie5WAGmA0WgFJQDlaAtWAD2AK2gV3gR7AfNIDD4Dj4FZwDF8FVcAuuny7wFPSC16AfQRASQkcYiDFigdgizog7wkYCkTAkGolHUpEMJBuRIkpkNrIQKUdWIRuQrUgN8hNyCDmOnEHakZvIPaQbeYG8RzGUhhqgZqgdOgZloxw0Ck1EJ6PZ6HS0GF2ELkMr0Wp0D1qPHkfPoVfRDvQp2ocBTAtjYpaYC8bGuFgsloZlYXJsLlaGVWDVWB3WBP/py1gH1oO9w4k4A2fhLnANR+JJuACfjs/Fl+Ib8F14Pd6KX8bv4b34JwKdYEpwJvgReISJhGzCDEIpoYKwg3CQcBLupi7CayKRyCTaE33gbkwl5hBnEZcSNxH3EpuJ7cROYh+JRDImOZMCSLEkPqmAVEpaT9pDOka6ROoivSVrkS3I7uRwchpZSi4hV5B3k4+SL5EfkfspuhRbih8lliKkzKQsp2ynNFEuULoo/VQ9qj01gJpIzaEuoFZS66gnqbepL7W0tKy0fLUmaEm05mtVau3TOq11T+sdTZ/mROPS0mlK2jLaTloz7SbtJZ1Ot6MH09PoBfRl9Br6Cfpd+ltthvZobZ62UHuedpV2vfYl7Wc6FB1bHY7OFJ1inQqdAzoXdHp0Kbp2ulxdvu5c3SrdQ7rXdfv0GHpuerF6+XpL9XbrndF7rE/St9MP0xfqL9Lfpn9Cv5OBMawZXIaAsZCxnXGS0WVANLA34BnkGJQb/GjQZtBrqG/oaZhsWGRYZXjEsIOJMe2YPGYeczlzP/Ma8/0IsxGcEaIRS0bUjbg04o3RSKNgI5FRmdFeo6tG741ZxmHGucYrjRuM75jgJk4mE0xmmGw2OWnSM9JgpP9IwciykftH/m6KmjqZxpvOMt1met60z8zcLMJMZrbe7IRZjznTPNg8x3yN+VHzbguGRaCFxGKNxTGLJyxDFoeVx6pktbJ6LU0tIy2Vllst2yz7reytkqxKrPZa3bGmWrOts6zXWLdY99pY2Iy3mW1Ta/O7LcWWbSu2XWd7yvaNnb1dit23dg12j+2N7Hn2xfa19rcd6A5BDtMdqh2uOBId2Y65jpscLzqhTl5OYqcqpwvOqLO3s8R5k3P7KMIo31HSUdWjrrvQXDguhS61LvdGM0dHjy4Z3TD62RibMWljVo45NeaTq5drnut211tu+m7j3ErcmtxeuDu5C9yr3K940D3CPeZ5NHo893T2FHlu9rzhxfAa7/WtV4vXR28fb7l3nXe3j41Phs9Gn+tsA3Yceyn7tC/BN8R3nu9h33d+3n4Ffvv9/vJ38c/13+3/eKz9WNHY7WM7A6wC+AFbAzoCWYEZgd8HdgRZBvGDqoPuB1sHC4N3BD/iOHJyOHs4z0JcQ+QhB0PecP24c7jNoVhoRGhZaFuYflhS2Iawu+FW4dnhteG9EV4RsyKaIwmRUZErI6/zzHgCXg2vd5zPuDnjWqNoUQlRG6LuRztFy6ObxqPjx41fPf52jG2MNKYhFsTyYlfH3omzj5se98sE4oS4CVUTHsa7xc+OP5XASJiasDvhdWJI4vLEW0kOScqklmSd5PTkmuQ3KaEpq1I6Jo6ZOGfiuVSTVElqYxopLTltR1rfpLBJayd1pXull6Zfm2w/uWjymSkmU/KmHJmqM5U/9UAGISMlY3fGB34sv5rfl8nL3JjZK+AK1gmeCoOFa4TdogDRKtGjrICsVVmPswOyV2d3i4PEFeIeCVeyQfI8JzJnS86b3NjcnbkDeSl5e/PJ+Rn5h6T60lxp6zTzaUXT2mXOslJZx3S/6Wun98qj5DsUiGKyorHAAB7ezysdlN8o7xUGFlYVvp2RPONAkV6RtOj8TKeZS2Y+Kg4v/mEWPkswq2W25ewFs+/N4czZOheZmzm3ZZ71vEXzuuZHzN+1gLogd8FvJa4lq0peLUxZ2LTIbNH8RZ3fRHxTW6pdKi+9/q3/t1sW44sli9uWeCxZv+RTmbDsbLlreUX5h6WCpWe/c/uu8ruBZVnL2pZ7L9+8grhCuuLayqCVu1bprSpe1bl6/Or6Naw1ZWterZ269kyFZ8WWddR1ynUdldGVjett1q9Y/2GDeMPVqpCqvRtNNy7Z+GaTcNOlzcGb67aYbSnf8v57yfc3tkZsra+2q67YRtxWuO3h9uTtp35g/1Czw2RH+Y6PO6U7O3bF72qt8amp2W26e3ktWqus7d6Tvufij6E/Nta51G3dy9xbvg/sU+578lPGT9f2R+1vOcA+UPez7c8bDzIOltUj9TPrexvEDR2NqY3th8Ydamnybzr4y+hfdh62PFx1xPDI8qPUo4uODhwrPtbXLGvuOZ59vLNlasutExNPXGmd0Np2Murk6V/Dfz1xinPq2OmA04fP+J05dJZ9tuGc97n6817nD/7m9dvBNu+2+gs+Fxov+l5sah/bfvRS0KXjl0Mv/3qFd+Xc1Zir7deSrt24nn6944bwxuObeTef/174e/+t+bcJt8vu6N6puGt6t/oPxz/2dnh3HLkXeu/8/YT7tzoFnU8fKB586Fr0kP6w4pHFo5rH7o8Pd4d3X3wy6UnXU9nT/p7SP/X+3PjM4dnPfwX/db53Ym/Xc/nzgRdLXxq/3PnK81VLX1zf3df5r/vflL01frvrHfvdqfcp7x/1z/hA+lD50fFj06eoT7cH8gcGZHw5f/AogMGKZmUB8GInAPRUABgX4flhkvrON1gQ9T11kMB/YvW9cLB4A1AHG9VxndsMwD5Y7WClzwdAdVRPDAaoh8dw1RRFloe72hYN3ngIbwcGXpoBQGoC4KN8YKB/08DAR3hHxW4C0DxdfddUFSK8G3wfoKKrRsL54Kuivod+EePXLVB54Am+bv8F0nSIcoyVGVcAAABsZVhJZk1NACoAAAAIAAQBGgAFAAAAAQAAAD4BGwAFAAAAAQAAAEYBKAADAAAAAQACAACHaQAEAAAAAQAAAE4AAAAAAAAAkAAAAAEAAACQAAAAAQACoAIABAAAAAEAAAA0oAMABAAAAAEAAAAyAAAAALjV8aUAAAAJcEhZcwAAFiUAABYlAUlSJPAAAASiSURBVFgJ7Zhrd5pIHMajgoCoeEk0BlDTXHq6zfb7f4y227zYtGka4x2rolEhXOxDJk1cDJEg9uTsybzwMCMz8+OZ/w0i8/l866W26EsFc7he4YKezotWjgr6VI/Pg3uhmYahz2a4g+E4iqYjt+3xCU+Ohg9nmtb1aNytX2HfgiQnBYGiYsB7EuPxP0ODI5rNNH04HCntVuPiAhtq88iOYWUyaY5lAugXJhw0A9m/X89rl5dKqwk4ZaKXx9O3Rwf0di6AfiHALWpWu4JkP1qtlj6dAE5vNC17zrEsrgPoFw7cvWYgazTqhqZl+SSABpNrdHEx07QA+q0Ft6xZp93eMm9yqaQsStFYjOkp4+lMURTwQT84RTYrcJwjpJ+2LpxLM5CV8hlZFPePjuNcQh4MruqNs7NvRD9d19+/O9443LJm9Xp9pA5YKmZbNmIbn06ls7l4gsOBfj07U4cDFBgsy1YrUt7O+vTcgMoBzqUZyAzDsE2z1u5GGC4vyrE4a1n2bYSL4C/coA772kzDRJ+e+2y4Zc1gUnGa3tnZhhnh4EbqqK30Ln5cTqZOklC6CgZpihLSaSGdouln7BjBZn5s8/4e24Y6ltLrI54R3wTZ8fFhPp93UBTl9PQUfIIgMAyDEZCpqoruycn7w8MDWRIFIR3+sXpptlvc2a9WirtFoCQTXL/buZlOB/2+YZoYASLIKpXywcEbWZZ4PhGN+q02niHysp0RzUAminuplBPYbF17I5XmugbLU68dOKIZyPb2SiCjYjEM+my+4J7WrFyWU0meoijTsracBH+X4xc1wz0gw8P4xCK3+YVz+eZ/NPtNNplMlZ+D7/UWZJtqejaXg50F08wX3LJmdzkgn6nI0qJmqjpqNltwUriqZpggI3YWTDO/cC7NSA7Yl0rFbWSpu9OEZiD7+PHT5WXt3jfX0WwF3LJmqDWQ0W/zpliWZVg6sTOi2fn593uy9TVbDefSjNQayOjImyVJ5HgeHrAhzVbA2fb85saAKvV6o9lqIe2kEpwkitVqtbBb4tNpkG1OsxVwlmUhZ4/Go0G/N5tMoFNJFN+e/FWWRT6V3LRmK+DsuW2YBpLPbDbFD8OyFE2h1uBSvL01H6tjeMAm7IxgrYBbvInUFP2fveFwSDJBu90O3TcXd3wGnG1ZumUNB4NaraZpGmZ2u93QfTMgHJmGAPblyynDfEN3sdZYP54tY5ERz/QVQZp02u9MeQsEwci09fOmF9DiuCccoO7oHvAeJq5TazyssurKEy4ajTLxeEYQymWJZZ2ycbEVCjtHR4eSJAaoNRbXefrasxJ2Kl6EuulMHam6prtWYVhGSAtcgkN9hsdw/RtW1xOO5FbkCURjxLyw9nOtE43g7RaPRyzI/bHH81gBh9yKDIY8gWjsWjSsLk3ReNmOx/EC9MiXKE84kluHqtrpdq+vnQ8fm2jJJF8sFGDZsI1l6/CEI7kVZJ8//9PpdDZBhjWLxeKHD38jMOGVEeK5dtmULbu2Cdb1dAgYnIYvgX/qWBGtlpXzhCMvz3/SIZaNzhPuJYQST7hgVhLurBftEK9wQQ/7Vbn/pXK/AE4OtgYO+7i0AAAAAElFTkSuQmCC"/>
                <span>修改 OKR</span>
              </p>
            </div>
          </div>
        </div>
        <div class="bottom-btn">
          <span class="confirm-submit">发布</span>
          <span class="cancel-submit">取消</span>
        </div>
        <div class="bottom-remind-info">
          <span>进度记录：保持更新，让小伙伴们看到你的进度</span>
        </div>
        <div class="current-item-id">${primaryObject.id}</div>
        `

      primaryObject.kr.forEach((el, idx) => {
        htmr +=
            `<li>
            <p class="left">
              <span>KR${idx + 1}</span>
              <input disabled="true" class="keyInput" type="text" placeholder="请输入Key Result的值" value="${el.content}">
            </p>
            <p class="right">
              <input disabled="true" class="progress-val" value="${el.progress}%"></input>
              <input disabled="true" class="scores" value="${el.score}"></input>
              <input disabled="true" class="weights" value="${el.weight}%"></input>
              <span class="delete">×</span>
            </p>
          </li > 
          `
      })
      // 放入原来的值
      $(curretnDeleteObjective).html(htm);
      // 添加限制的类名
      $(curretnDeleteObjective).addClass('allow-change-info');
      // 再将原来的key值放进去
      $($(curretnDeleteObjective).find('ul')[0]).append(htmr);
      // 关闭提醒弹框
      $('.remind-dialog-box').css('display', 'none');
      // 重置状态
      cancelSubmitCode = false;
    } else {
      // 服务器请求回来的需要带id删除
      deleteCurrentObjective($(curretnDeleteObjectiveId[0]).html());
    }
  }
})

// 9.共用的提醒信息弹框
function remindInfoBox(val, timer = 2000) {
  $('.remind-info-dialog-box').css('display', 'block');
  $('.remind-info-dialog').html(val);
  setTimeout(() => {
    $('.remind-info-dialog-box').css('display', 'none');
  }, timer)
}

function remindSuccessInfoBox(val, timer = 2000) {
  $('.remind-info-success-dialog-box').css('display', 'block');
  $('.remind-info-success-dialog').html(val);
  setTimeout(() => {
    $('.remind-info-success-dialog-box').css('display', 'none');
  }, timer)
}

// 当前选择的日期时间的对象，方便拿到id
let currentDateId;
// 10.点击时间向左
$(document).on("click", '.to-left', function (el) {
  // 拿到当前Input元素所在的ul
  const currentTarget = $(el.target).parents('.right-date')[0];
  // 拿到当前Input元素所在的ul对应的input
  const leftInput = $(currentTarget).find('.dataLeftInput')[0];
  const rightInput = $(currentTarget).find('.dataRightInput')[0];

  // 如果向右标签含有禁止符号，则停止
  if ($(el.currentTarget).hasClass('special-style')) return

  // 时间对象只有1个就不需要处理
  if (okrDateList.length < 2) {
    $($(currentTarget).find('.to-left')[0]).addClass('special-style');
    return
  }

  // 时间对象等于2个时
  if (okrDateList.length == 2) {
    $(rightInput).removeClass('special-input');
    $(leftInput).addClass('special-input');
    // 移除右侧不可点状态
    $($(currentTarget).find('.to-right')[0]).removeClass('special-style');
    // 添加左侧不可点状态
    $($(currentTarget).find('.to-left')[0]).addClass('special-style');
  }

  // 时间对象大于2个时
  if (okrDateList.length > 2) {
    // 当点击到最右侧后第一次点击向左时处理
    if (rightIndex == okrDateList.length - 1 && $($(currentTarget).find('.to-right')[0]).hasClass('special-style')) {
      $($(currentTarget).find('.to-right')[0]).removeClass('special-style');
      $(rightInput).removeClass('special-input');
      $(leftInput).addClass('special-input');
      currentDateId = okrDateList.find(el => el.name == $('.special-input').val());
      getCurrentDateData(currentDateId.id);
      return
    }
    // 移除右侧不可点状态
    $($(currentTarget).find('.to-right')[0]).removeClass('special-style');
    leftIndex--;
    rightIndex--;
    // 给左右侧赋新值
    $(leftInput).val(okrDateList[leftIndex].name);
    $(rightInput).val(okrDateList[rightIndex].name);
    // 只要到最左边，就马上给不能点图标
    if (leftIndex == 0) {
      $($(currentTarget).find('.to-left')[0]).addClass('special-style');
    }
  }

  // 拿到当前活跃时间值
  currentDateId = okrDateList.find(el => el.name == $('.special-input').val());

  // 请求新的数据
  getCurrentDateData(currentDateId.id);
})

// 11.点击时间向右
$(document).on("click", '.to-right', function (el) {
  // 拿到当前Input元素所在的ul
  const currentTarget = $(el.target).parents('.right-date')[0];
  // 拿到当前Input元素所在的ul对应的input
  const leftInput = $(currentTarget).find('.dataLeftInput')[0];
  const rightInput = $(currentTarget).find('.dataRightInput')[0];

  // 如果向右标签含有禁止符号，则停止
  if ($(el.currentTarget).hasClass('special-style')) return

  // 时间对象只有1个就不需要处理
  if (okrDateList.length < 2) {
    $($(currentTarget).find('.to-right')[0]).addClass('special-style');
    return
  }

  // 时间对象等于2个时
  if (okrDateList.length == 2) {
    $(rightInput).addClass('special-input');
    $(leftInput).removeClass('special-input');
    // 移除左侧不可点状态
    $($(currentTarget).find('.to-left')[0]).removeClass('special-style');
    // 添加右侧不可点状态
    $($(currentTarget).find('.to-right')[0]).addClass('special-style');
  }

  // 时间对象大于2个时
  if (okrDateList.length > 2) {
    // 向右点击，当显示了最后的两个数据时，不再++，直接处理数据
    if (rightIndex == okrDateList.length - 1) {
      $($(currentTarget).find('.to-right')[0]).addClass('special-style');
      $(rightInput).addClass('special-input');
      $(leftInput).removeClass('special-input');
      currentDateId = okrDateList.find(el => el.name == $('.special-input').val());
      getCurrentDateData(currentDateId.id)
      return
    }
    // 移除左侧不可点状态
    $($(currentTarget).find('.to-left')[0]).removeClass('special-style');
    // 给左右侧赋新值
    leftIndex++;
    rightIndex++;
    $(leftInput).val(okrDateList[leftIndex].name);
    $(rightInput).val(okrDateList[rightIndex].name);
  }

  // 拿到当前活跃时间值
  currentDateId = okrDateList.find(el => el.name == $('.special-input').val());

  // 请求新的数据
  getCurrentDateData(currentDateId.id);
})

// 12.获取日期列表
let okrDateList = [];
let leftIndex = 0;
let rightIndex = 1;
function getDateList() {
  $.get(createLink('okr', 'ajax_get_period'), function (data) {
    if (+data.code === 200) {
      // console.log(data)
      // 默认请求第一个时间的数据
      getCurrentDateData(data.data[0].id);
      // 将第一个值赋值给共用变量（当前选中的时间）
      currentDateId = data.data[0];
      // 拿到获取的所有日期
      okrDateList = [...data.data];
      let htm;
      if (okrDateList.length > 1) {
        htm =
            `
          <p class="to-left special-style">
            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkZDNTZDNkNBQ0YzMTExRTc4OTdFRkU5REE5MDE2MkVBIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkZDNTZDNkNCQ0YzMTExRTc4OTdFRkU5REE5MDE2MkVBIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6RkM1NkM2QzhDRjMxMTFFNzg5N0VGRTlEQTkwMTYyRUEiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6RkM1NkM2QzlDRjMxMTFFNzg5N0VGRTlEQTkwMTYyRUEiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4XzjmcAAACOUlEQVR42uzby0rDQBQG4FZrEXHjY/gQCoJLqwiKoFB1UW8g6ELQIoQsil241NaNgvWG4EbrWkHwGXwIcScuFK3/YAJFmmQgM3OmpznwE8i0KR9Nk7mk6Uajkeqk6kp1WCXgBMysMnEP4LpurPc7jhN27A1sdpEBpI4U8PpXUrCuAraETbFp1wQibimT7E5pYLf/Yf3KsfsNe9i9gOZ3VuAIrKh9NmAJ7BFSYgGWxK7hCh27H5yO25eOe1sSEOTQBNaGb3g1AnusEksNFthKSPslsqwSSwmWweaB/ebQlybDUoAXKLGmwXPeRYgMaxIssLWQzxMjoUXdWFNgGewUsJ8cJgCswuoGz9iG1QkeR86isIhRrC6wwN4gPQHtD1RYHeAo7KM3a0GCVQ2WwY4hH5Sjla5OwqoCDyPX7YBVBRYD9N6AtmebsKrAgwH7v5BZm7CqwC8B+8Upfo70cQOvhNxmhpB7m9AqwE/ItHcKt6oRm9Cqbkt3Xu/JerTKjkdboFV3LWXQt0iWC1gGPer1yrJcwD56HvkJaM9RoXVOAFwh+Si067pZLmBRF7ahTUziyaIzXMCy6FOgu7mAfXQhpF0MNGq60aaXWk5Sf+vBZGiKxbQqJZpquZQMTbkgLoOuAp3mAvbR6yHt4iJXUYkmf4rHcZwDbHYiJhiUoa14bAnosim0NQ+mSaKLbMCS6C1WYAl0PztwE7rcoqnOEuyhxbe8ibx5gw4xqbAU97jp5H9LCTgBt3X9CjAA4H3MDFxaVIwAAAAASUVORK5CYII=" alt="">
            <input disabled="true" class="dataLeftInput special-input" value="${okrDateList[leftIndex].name}" type="text">
          </p>
          <p class="to-right">
            <input disabled="true" class="dataRightInput" value="${okrDateList[rightIndex].name}" type="text">
            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAAAXNSR0IArs4c6QAAAH5lWElmTU0AKgAAAAgAAwESAAMAAAABAAEAAAExAAIAAAAiAAAAModpAAQAAAABAAAAVAAAAABBZG9iZSBQaG90b3Nob3AgQ0MgMjAxNyAoV2luZG93cykAAAOgAQADAAAAAQABAACgAgAEAAAAAQAAADygAwAEAAAAAQAAADwAAAAA/44fhwAABBVpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IlhNUCBDb3JlIDUuNC4wIj4KICAgPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICAgICAgPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIKICAgICAgICAgICAgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iCiAgICAgICAgICAgIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIgogICAgICAgICAgICB4bWxuczp0aWZmPSJodHRwOi8vbnMuYWRvYmUuY29tL3RpZmYvMS4wLyIKICAgICAgICAgICAgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIj4KICAgICAgICAgPHhtcE1NOkluc3RhbmNlSUQ+eG1wLmlpZDpGQzU2QzZDQUNGMzExMUU3ODk3RUZFOURBOTAxNjJFQTwveG1wTU06SW5zdGFuY2VJRD4KICAgICAgICAgPHhtcE1NOkRlcml2ZWRGcm9tIHJkZjpwYXJzZVR5cGU9IlJlc291cmNlIj4KICAgICAgICAgICAgPHN0UmVmOmluc3RhbmNlSUQ+eG1wLmlpZDpGQzU2QzZDOENGMzExMUU3ODk3RUZFOURBOTAxNjJFQTwvc3RSZWY6aW5zdGFuY2VJRD4KICAgICAgICAgICAgPHN0UmVmOmRvY3VtZW50SUQ+eG1wLmRpZDpGQzU2QzZDOUNGMzExMUU3ODk3RUZFOURBOTAxNjJFQTwvc3RSZWY6ZG9jdW1lbnRJRD4KICAgICAgICAgPC94bXBNTTpEZXJpdmVkRnJvbT4KICAgICAgICAgPHhtcE1NOkRvY3VtZW50SUQ+eG1wLmRpZDpGQzU2QzZDQkNGMzExMUU3ODk3RUZFOURBOTAxNjJFQTwveG1wTU06RG9jdW1lbnRJRD4KICAgICAgICAgPHRpZmY6T3JpZW50YXRpb24+MTwvdGlmZjpPcmllbnRhdGlvbj4KICAgICAgICAgPHhtcDpDcmVhdG9yVG9vbD5BZG9iZSBQaG90b3Nob3AgQ0MgMjAxNyAoV2luZG93cyk8L3htcDpDcmVhdG9yVG9vbD4KICAgICAgPC9yZGY6RGVzY3JpcHRpb24+CiAgIDwvcmRmOlJERj4KPC94OnhtcG1ldGE+CsWdTrAAAAOESURBVGgF7ZpJixNBFMeTGc1BvIn4EcQ5efDmJZHx5naQCK6HoLjgQRAZUUgCkhE86EHGBVEZUDCMBzc8JhnnGyjxIH4E8SKikUn8/yElNaGq6bZWpR88uvpVdfX71fK6u7oKhVzyFshb4F9ugaKp861Wa/NgMHiAevZAv0KvNRqNW6b1urp+yrRiwN5HHfugrGsT9CaA53GMUoyBQbVXQTYH6DmFPbjJBvA3DcV8jNDGwMVi8YYGmObooKcTnE2V1el0Vnq93hYU3qG5YLZcLv/sdrsrmnyvZuMoTW9Ho1Gx2WwuIHk6wfvLGOLXE/K9ZFkBpqcpoc8D+rYXMs1NrAFL0PeQPqm5XwFz/my9Xr+jy3dtNw5asoOAGc3MzJyB7alsl9MYCQsY/iwTRKz2sCBot9vT/X5/EeeHhW3yGKqnnQATLlZoZ8BpoVGuhkD2kOV9iFNgAqTo6SGG93EEsic+gK0GLZXD1Wp1FfYT0FeqfNimEMgWEciOaPKtmo3ftNJ4g7esId62nqPsduhWxTUcaQcqlcpnlH2vyLdm8gJMbwGyGgO0N+CM0J/QQB+sdatUkfOgJd3rTxJRuYSTJajqW5rlfkEPotxLntiUIMAEGEO/QXJWA+QE2nmU1sAQeIC8/dCOpsx62JdQjstH1iRYDwsCAG1A+jW0ImwTR6s9HRyYcD6howBOCc0psBuNs8zyfyvB5vCkwwD5DhvXtt9N5o3PGdnvavJSm6MBpsdj6KNIct6qZJvKmMUWGzADGBcPGKFV8lFlzGKLBhi9K6L1Tg3AD9iTFgk1l601RwEswSY9mg6h3PJa97OfBY/SKWGtvWYGBfYNy/EQDBiwfMy8he6iIwqx+oYl6g8yh8ew/FryCkvodYLc11GC1X0aco3rGNa4rH8aktFrD6eE5YLeM1cd4G0OZ4B1unrpBTgWWC9DmuvSuNEjaNKc/T/WpVMswrPRa74W4XkzZ1E6Dez4h9pjOuJLnMzhDLDe/xNbB44ZlqPI6nOY2x7wX5g7AKL7LyymjDVgaY9HTVSuOJ5DgPI+jGU/rABLsEkf6NzFw50+QcV4DmeADb5liS1t/DMNj5arqOdSQrdFsT9L+GdjSF8UlSmOUcHSPxvAGxWgNEUHS6dsAKu2MrQQoKKYs4SUxRi4VCqdQoUvoEPoF+gFwF7BMZe8BfIWyFsgbwHTFvgN9H9NsbJyclYAAAAASUVORK5CYII=" alt="">
          </p>
        `
      } else if (okrDateList.length == 1) {
        htm =
            `
          <p class="to-left special-style">
            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkZDNTZDNkNBQ0YzMTExRTc4OTdFRkU5REE5MDE2MkVBIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkZDNTZDNkNCQ0YzMTExRTc4OTdFRkU5REE5MDE2MkVBIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6RkM1NkM2QzhDRjMxMTFFNzg5N0VGRTlEQTkwMTYyRUEiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6RkM1NkM2QzlDRjMxMTFFNzg5N0VGRTlEQTkwMTYyRUEiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4XzjmcAAACOUlEQVR42uzby0rDQBQG4FZrEXHjY/gQCoJLqwiKoFB1UW8g6ELQIoQsil241NaNgvWG4EbrWkHwGXwIcScuFK3/YAJFmmQgM3OmpznwE8i0KR9Nk7mk6Uajkeqk6kp1WCXgBMysMnEP4LpurPc7jhN27A1sdpEBpI4U8PpXUrCuAraETbFp1wQibimT7E5pYLf/Yf3KsfsNe9i9gOZ3VuAIrKh9NmAJ7BFSYgGWxK7hCh27H5yO25eOe1sSEOTQBNaGb3g1AnusEksNFthKSPslsqwSSwmWweaB/ebQlybDUoAXKLGmwXPeRYgMaxIssLWQzxMjoUXdWFNgGewUsJ8cJgCswuoGz9iG1QkeR86isIhRrC6wwN4gPQHtD1RYHeAo7KM3a0GCVQ2WwY4hH5Sjla5OwqoCDyPX7YBVBRYD9N6AtmebsKrAgwH7v5BZm7CqwC8B+8Upfo70cQOvhNxmhpB7m9AqwE/ItHcKt6oRm9Cqbkt3Xu/JerTKjkdboFV3LWXQt0iWC1gGPer1yrJcwD56HvkJaM9RoXVOAFwh+Si067pZLmBRF7ahTUziyaIzXMCy6FOgu7mAfXQhpF0MNGq60aaXWk5Sf+vBZGiKxbQqJZpquZQMTbkgLoOuAp3mAvbR6yHt4iJXUYkmf4rHcZwDbHYiJhiUoa14bAnosim0NQ+mSaKLbMCS6C1WYAl0PztwE7rcoqnOEuyhxbe8ibx5gw4xqbAU97jp5H9LCTgBt3X9CjAA4H3MDFxaVIwAAAAASUVORK5CYII=" alt="">
            <input disabled="true" class="dataLeftInput special-input" value="${okrDateList[leftIndex].name}" type="text">
          </p>
          <p class="to-right special-style">
            <input disabled="true" class="dataLeftInput special-input" value="${okrDateList[leftIndex].name}" type="text">
            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAAAXNSR0IArs4c6QAAAH5lWElmTU0AKgAAAAgAAwESAAMAAAABAAEAAAExAAIAAAAiAAAAModpAAQAAAABAAAAVAAAAABBZG9iZSBQaG90b3Nob3AgQ0MgMjAxNyAoV2luZG93cykAAAOgAQADAAAAAQABAACgAgAEAAAAAQAAADygAwAEAAAAAQAAADwAAAAA/44fhwAABBVpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IlhNUCBDb3JlIDUuNC4wIj4KICAgPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICAgICAgPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIKICAgICAgICAgICAgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iCiAgICAgICAgICAgIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIgogICAgICAgICAgICB4bWxuczp0aWZmPSJodHRwOi8vbnMuYWRvYmUuY29tL3RpZmYvMS4wLyIKICAgICAgICAgICAgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIj4KICAgICAgICAgPHhtcE1NOkluc3RhbmNlSUQ+eG1wLmlpZDpGQzU2QzZDQUNGMzExMUU3ODk3RUZFOURBOTAxNjJFQTwveG1wTU06SW5zdGFuY2VJRD4KICAgICAgICAgPHhtcE1NOkRlcml2ZWRGcm9tIHJkZjpwYXJzZVR5cGU9IlJlc291cmNlIj4KICAgICAgICAgICAgPHN0UmVmOmluc3RhbmNlSUQ+eG1wLmlpZDpGQzU2QzZDOENGMzExMUU3ODk3RUZFOURBOTAxNjJFQTwvc3RSZWY6aW5zdGFuY2VJRD4KICAgICAgICAgICAgPHN0UmVmOmRvY3VtZW50SUQ+eG1wLmRpZDpGQzU2QzZDOUNGMzExMUU3ODk3RUZFOURBOTAxNjJFQTwvc3RSZWY6ZG9jdW1lbnRJRD4KICAgICAgICAgPC94bXBNTTpEZXJpdmVkRnJvbT4KICAgICAgICAgPHhtcE1NOkRvY3VtZW50SUQ+eG1wLmRpZDpGQzU2QzZDQkNGMzExMUU3ODk3RUZFOURBOTAxNjJFQTwveG1wTU06RG9jdW1lbnRJRD4KICAgICAgICAgPHRpZmY6T3JpZW50YXRpb24+MTwvdGlmZjpPcmllbnRhdGlvbj4KICAgICAgICAgPHhtcDpDcmVhdG9yVG9vbD5BZG9iZSBQaG90b3Nob3AgQ0MgMjAxNyAoV2luZG93cyk8L3htcDpDcmVhdG9yVG9vbD4KICAgICAgPC9yZGY6RGVzY3JpcHRpb24+CiAgIDwvcmRmOlJERj4KPC94OnhtcG1ldGE+CsWdTrAAAAOESURBVGgF7ZpJixNBFMeTGc1BvIn4EcQ5efDmJZHx5naQCK6HoLjgQRAZUUgCkhE86EHGBVEZUDCMBzc8JhnnGyjxIH4E8SKikUn8/yElNaGq6bZWpR88uvpVdfX71fK6u7oKhVzyFshb4F9ugaKp861Wa/NgMHiAevZAv0KvNRqNW6b1urp+yrRiwN5HHfugrGsT9CaA53GMUoyBQbVXQTYH6DmFPbjJBvA3DcV8jNDGwMVi8YYGmObooKcTnE2V1el0Vnq93hYU3qG5YLZcLv/sdrsrmnyvZuMoTW9Ho1Gx2WwuIHk6wfvLGOLXE/K9ZFkBpqcpoc8D+rYXMs1NrAFL0PeQPqm5XwFz/my9Xr+jy3dtNw5asoOAGc3MzJyB7alsl9MYCQsY/iwTRKz2sCBot9vT/X5/EeeHhW3yGKqnnQATLlZoZ8BpoVGuhkD2kOV9iFNgAqTo6SGG93EEsic+gK0GLZXD1Wp1FfYT0FeqfNimEMgWEciOaPKtmo3ftNJ4g7esId62nqPsduhWxTUcaQcqlcpnlH2vyLdm8gJMbwGyGgO0N+CM0J/QQB+sdatUkfOgJd3rTxJRuYSTJajqW5rlfkEPotxLntiUIMAEGEO/QXJWA+QE2nmU1sAQeIC8/dCOpsx62JdQjstH1iRYDwsCAG1A+jW0ImwTR6s9HRyYcD6howBOCc0psBuNs8zyfyvB5vCkwwD5DhvXtt9N5o3PGdnvavJSm6MBpsdj6KNIct6qZJvKmMUWGzADGBcPGKFV8lFlzGKLBhi9K6L1Tg3AD9iTFgk1l601RwEswSY9mg6h3PJa97OfBY/SKWGtvWYGBfYNy/EQDBiwfMy8he6iIwqx+oYl6g8yh8ew/FryCkvodYLc11GC1X0aco3rGNa4rH8aktFrD6eE5YLeM1cd4G0OZ4B1unrpBTgWWC9DmuvSuNEjaNKc/T/WpVMswrPRa74W4XkzZ1E6Dez4h9pjOuJLnMzhDLDe/xNbB44ZlqPI6nOY2x7wX5g7AKL7LyymjDVgaY9HTVSuOJ5DgPI+jGU/rABLsEkf6NzFw50+QcV4DmeADb5liS1t/DMNj5arqOdSQrdFsT9L+GdjSF8UlSmOUcHSPxvAGxWgNEUHS6dsAKu2MrQQoKKYs4SUxRi4VCqdQoUvoEPoF+gFwF7BMZe8BfIWyFsgbwHTFvgN9H9NsbJyclYAAAAASUVORK5CYII=" alt="">
          </p>
        `
      } else {
        htm =
            `
          <p class="to-left special-style">
            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkZDNTZDNkNBQ0YzMTExRTc4OTdFRkU5REE5MDE2MkVBIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkZDNTZDNkNCQ0YzMTExRTc4OTdFRkU5REE5MDE2MkVBIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6RkM1NkM2QzhDRjMxMTFFNzg5N0VGRTlEQTkwMTYyRUEiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6RkM1NkM2QzlDRjMxMTFFNzg5N0VGRTlEQTkwMTYyRUEiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4XzjmcAAACOUlEQVR42uzby0rDQBQG4FZrEXHjY/gQCoJLqwiKoFB1UW8g6ELQIoQsil241NaNgvWG4EbrWkHwGXwIcScuFK3/YAJFmmQgM3OmpznwE8i0KR9Nk7mk6Uajkeqk6kp1WCXgBMysMnEP4LpurPc7jhN27A1sdpEBpI4U8PpXUrCuAraETbFp1wQibimT7E5pYLf/Yf3KsfsNe9i9gOZ3VuAIrKh9NmAJ7BFSYgGWxK7hCh27H5yO25eOe1sSEOTQBNaGb3g1AnusEksNFthKSPslsqwSSwmWweaB/ebQlybDUoAXKLGmwXPeRYgMaxIssLWQzxMjoUXdWFNgGewUsJ8cJgCswuoGz9iG1QkeR86isIhRrC6wwN4gPQHtD1RYHeAo7KM3a0GCVQ2WwY4hH5Sjla5OwqoCDyPX7YBVBRYD9N6AtmebsKrAgwH7v5BZm7CqwC8B+8Upfo70cQOvhNxmhpB7m9AqwE/ItHcKt6oRm9Cqbkt3Xu/JerTKjkdboFV3LWXQt0iWC1gGPer1yrJcwD56HvkJaM9RoXVOAFwh+Si067pZLmBRF7ahTUziyaIzXMCy6FOgu7mAfXQhpF0MNGq60aaXWk5Sf+vBZGiKxbQqJZpquZQMTbkgLoOuAp3mAvbR6yHt4iJXUYkmf4rHcZwDbHYiJhiUoa14bAnosim0NQ+mSaKLbMCS6C1WYAl0PztwE7rcoqnOEuyhxbe8ibx5gw4xqbAU97jp5H9LCTgBt3X9CjAA4H3MDFxaVIwAAAAASUVORK5CYII=" alt="">
          <input disabled="true" class="dataLeftInput" value="" type="text">
          </p>
          <p class="to-right special-style">
          <input disabled="true" class="dataRightInput" value="" type="text">
            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAAAXNSR0IArs4c6QAAAH5lWElmTU0AKgAAAAgAAwESAAMAAAABAAEAAAExAAIAAAAiAAAAModpAAQAAAABAAAAVAAAAABBZG9iZSBQaG90b3Nob3AgQ0MgMjAxNyAoV2luZG93cykAAAOgAQADAAAAAQABAACgAgAEAAAAAQAAADygAwAEAAAAAQAAADwAAAAA/44fhwAABBVpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IlhNUCBDb3JlIDUuNC4wIj4KICAgPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICAgICAgPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIKICAgICAgICAgICAgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iCiAgICAgICAgICAgIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIgogICAgICAgICAgICB4bWxuczp0aWZmPSJodHRwOi8vbnMuYWRvYmUuY29tL3RpZmYvMS4wLyIKICAgICAgICAgICAgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIj4KICAgICAgICAgPHhtcE1NOkluc3RhbmNlSUQ+eG1wLmlpZDpGQzU2QzZDQUNGMzExMUU3ODk3RUZFOURBOTAxNjJFQTwveG1wTU06SW5zdGFuY2VJRD4KICAgICAgICAgPHhtcE1NOkRlcml2ZWRGcm9tIHJkZjpwYXJzZVR5cGU9IlJlc291cmNlIj4KICAgICAgICAgICAgPHN0UmVmOmluc3RhbmNlSUQ+eG1wLmlpZDpGQzU2QzZDOENGMzExMUU3ODk3RUZFOURBOTAxNjJFQTwvc3RSZWY6aW5zdGFuY2VJRD4KICAgICAgICAgICAgPHN0UmVmOmRvY3VtZW50SUQ+eG1wLmRpZDpGQzU2QzZDOUNGMzExMUU3ODk3RUZFOURBOTAxNjJFQTwvc3RSZWY6ZG9jdW1lbnRJRD4KICAgICAgICAgPC94bXBNTTpEZXJpdmVkRnJvbT4KICAgICAgICAgPHhtcE1NOkRvY3VtZW50SUQ+eG1wLmRpZDpGQzU2QzZDQkNGMzExMUU3ODk3RUZFOURBOTAxNjJFQTwveG1wTU06RG9jdW1lbnRJRD4KICAgICAgICAgPHRpZmY6T3JpZW50YXRpb24+MTwvdGlmZjpPcmllbnRhdGlvbj4KICAgICAgICAgPHhtcDpDcmVhdG9yVG9vbD5BZG9iZSBQaG90b3Nob3AgQ0MgMjAxNyAoV2luZG93cyk8L3htcDpDcmVhdG9yVG9vbD4KICAgICAgPC9yZGY6RGVzY3JpcHRpb24+CiAgIDwvcmRmOlJERj4KPC94OnhtcG1ldGE+CsWdTrAAAAOESURBVGgF7ZpJixNBFMeTGc1BvIn4EcQ5efDmJZHx5naQCK6HoLjgQRAZUUgCkhE86EHGBVEZUDCMBzc8JhnnGyjxIH4E8SKikUn8/yElNaGq6bZWpR88uvpVdfX71fK6u7oKhVzyFshb4F9ugaKp861Wa/NgMHiAevZAv0KvNRqNW6b1urp+yrRiwN5HHfugrGsT9CaA53GMUoyBQbVXQTYH6DmFPbjJBvA3DcV8jNDGwMVi8YYGmObooKcTnE2V1el0Vnq93hYU3qG5YLZcLv/sdrsrmnyvZuMoTW9Ho1Gx2WwuIHk6wfvLGOLXE/K9ZFkBpqcpoc8D+rYXMs1NrAFL0PeQPqm5XwFz/my9Xr+jy3dtNw5asoOAGc3MzJyB7alsl9MYCQsY/iwTRKz2sCBot9vT/X5/EeeHhW3yGKqnnQATLlZoZ8BpoVGuhkD2kOV9iFNgAqTo6SGG93EEsic+gK0GLZXD1Wp1FfYT0FeqfNimEMgWEciOaPKtmo3ftNJ4g7esId62nqPsduhWxTUcaQcqlcpnlH2vyLdm8gJMbwGyGgO0N+CM0J/QQB+sdatUkfOgJd3rTxJRuYSTJajqW5rlfkEPotxLntiUIMAEGEO/QXJWA+QE2nmU1sAQeIC8/dCOpsx62JdQjstH1iRYDwsCAG1A+jW0ImwTR6s9HRyYcD6howBOCc0psBuNs8zyfyvB5vCkwwD5DhvXtt9N5o3PGdnvavJSm6MBpsdj6KNIct6qZJvKmMUWGzADGBcPGKFV8lFlzGKLBhi9K6L1Tg3AD9iTFgk1l601RwEswSY9mg6h3PJa97OfBY/SKWGtvWYGBfYNy/EQDBiwfMy8he6iIwqx+oYl6g8yh8ew/FryCkvodYLc11GC1X0aco3rGNa4rH8aktFrD6eE5YLeM1cd4G0OZ4B1unrpBTgWWC9DmuvSuNEjaNKc/T/WpVMswrPRa74W4XkzZ1E6Dez4h9pjOuJLnMzhDLDe/xNbB44ZlqPI6nOY2x7wX5g7AKL7LyymjDVgaY9HTVSuOJ5DgPI+jGU/rABLsEkf6NzFw50+QcV4DmeADb5liS1t/DMNj5arqOdSQrdFsT9L+GdjSF8UlSmOUcHSPxvAGxWgNEUHS6dsAKu2MrQQoKKYs4SUxRi4VCqdQoUvoEPoF+gFwF7BMZe8BfIWyFsgbwHTFvgN9H9NsbJyclYAAAAASUVORK5CYII=" alt="">
          </p>
        `
      }

      $('.right-date').append(htm);
    } else {
      remindInfoBox('请求失败！', 2000);
    }
  })
}

// 13.获取日期对应OKR
let dataList = [] // 所有的okr对象
function getCurrentDateData(val) {
  $.ajax({
    type: 'post',
    url: createLink('okr', 'ajax_get_period_data'),
    contentType: 'application/json',
    data: JSON.stringify({ id: val }),
    success: function (data) {
      if (data.code == 200) {
        // console.log(data.data)
        dataList = [...data.data];
        $('#objective-box').empty();
        // $('#objective-box').html('');
        dataList.forEach((item, index) => {
          let htm = '', htmr = '';
          htm =
              `
          <div class="objective-item allow-change-info">
            <div class="top-content">
              <div class="serial-number">
                <span>0${index + 1}</span>
              </div>
              <div class="content">
                <div class="add-align">
                  <div class="left">
                    <span class="open-dialog-box">+</span>
                    <span class="open-dialog-box">添加对齐</span>
                    <span class="alignment-content alignment-content-hidden">
                      <span class="cont-style">${item.alignment_owner}：</span>
                      <span class="cont-mes">${item.alignment_content}</span>
                    </span>
                    <div class="show-alignment-content-box"></div>
                  </div>
                  <div class="right">
                    <span>进度</span>
                    <span>分数</span>
                    <span>权重</span>
                  </div>
                </div>
                <div class="enter-key">
                  <ul>
                    <li>
                      <p class="left">
                        <input disabled="true" class="keyInput" type="text" placeholder="添加objective名字" value="${item.content}">
                      </p>
                      <p class="right">
                        <span class="default progress-value">${item.progress}%</span>
                        <span class="default scores-value">${item.score}</span>
                        <span class="default">${item.weight}%</span>
                        <span class="default-delete">×</span>
                      </p>
                    </li>
                  </ul>
                </div>
                <div class="add-key">
                  <p class="add">
                    <span>+</span>
                    <span>添加Key Result</span>
                  </p>
                  <p class="update">
                     <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADQAAAAyCAIAAACcQ2+hAAAMZWlDQ1BJQ0MgUHJvZmlsZQAASImVlwdYU8kWgOeWVBJaIAJSQm+idAJICaFFEJAqiEpIAgklxoQgYkeXVXDtIoplRVdFXHR1BWQtiLjWRbG7lkURFWVdLNhQeRMS0HVf+d5839z5c+bMmXNOZu6dAUCngy+T5aG6AORLC+TxESGsialpLFIXwAAZGAJ3MIYvUMg4cXHRAJah9u/l9TWAqNrLLipb/+z/r0VfKFIIAEDSIWcKFYJ8yM0A4CUCmbwAAGIolFvPKJCpWAzZQA4dhDxbxdlqXqHiTDVvH9RJjOdCbgSATOPz5dkAaLdCOatQkA3taD+A7CoVSqQA6BhADhSI+ULIiZBH5edPU/F8yA5QXwZ5F2R25hc2s/9mP3PYPp+fPczquAYLOVSikOXxZ/6fqfnfJT9POTSHHaw0sTwyXhU/zOGN3GlRKqZB7pFmxsSqcg35rUSozjsAKFWsjExS66OmAgUX5g8wIbsK+aFRkE0hh0vzYqI18swsSTgPMlwtaJGkgJeoGbtYpAhL0NjcKJ8WHzvEWXIuRzO2ji8fnFel36rMTeJo7N8Qi3hD9l8VixNTIFMBwKiFkuQYyNqQDRS5CVFqHcyqWMyNGdKRK+NV/ttAZoukESFq+1h6ljw8XqMvy1cMxYuViiW8GA1XFogTI9X5wXYL+IP+G0GuF0k5SUN2RIqJ0UOxCEWhYerYsTaRNEkTL3ZXVhASrxnbK8uL0+jjZFFehEpuBdlEUZigGYuPLYCLU20fj5YVxCWq/cQzcvjj4tT+4IUgGnBBKGABJayZYBrIAZK2noYe+EvdEw74QA6ygQi4aCRDI1IGe6TwmQCKwZ+QREAxPC5ksFcECqH847BU/XQBWYO9hYMjcsFDyPkgCuTB38rBUdLh2ZLBAyiR/GN2AfQ1D1ZV3z9lHCiJ1kiUQ3ZZOkOaxDBiKDGSGE50xE3wQNwfj4bPYFjdcTbuO+TtZ33CQ0I74T7hKqGDcHOqpET+lS/jQQe0H66JOPPLiHE7aNMLD8EDoHVoGWfiJsAF94TzcPAgOLMXlHI1fqtiZ/2bOIcj+CLnGj2KKwWljKAEUxy+HqntpO01bEWV0S/zo/Y1czir3OGer+fnfpFnIWyjvtbEFmMHsFPYcewMdhhrACzsGNaInceOqHh4DT0YXENDs8UP+pML7Uj+MR9fM6cqkwrXWtdu1w+aPlAgKipQbTDuNNlMuSRbXMDiwK+AiMWTCkaPYrm7ursBoPqmqF9TL5mD3wqEefazrOQ7AAI8BwYGDn+WResA8DPcG9TOzzIHP/g6KALg9DKBUl6oluGqBwG+DXTgjjIG5sAaOMCI3IE38AfBIAyMA7EgEaSCKTDPYrie5WAGmA0WgFJQDlaAtWAD2AK2gV3gR7AfNIDD4Dj4FZwDF8FVcAuuny7wFPSC16AfQRASQkcYiDFigdgizog7wkYCkTAkGolHUpEMJBuRIkpkNrIQKUdWIRuQrUgN8hNyCDmOnEHakZvIPaQbeYG8RzGUhhqgZqgdOgZloxw0Ck1EJ6PZ6HS0GF2ELkMr0Wp0D1qPHkfPoVfRDvQp2ocBTAtjYpaYC8bGuFgsloZlYXJsLlaGVWDVWB3WBP/py1gH1oO9w4k4A2fhLnANR+JJuACfjs/Fl+Ib8F14Pd6KX8bv4b34JwKdYEpwJvgReISJhGzCDEIpoYKwg3CQcBLupi7CayKRyCTaE33gbkwl5hBnEZcSNxH3EpuJ7cROYh+JRDImOZMCSLEkPqmAVEpaT9pDOka6ROoivSVrkS3I7uRwchpZSi4hV5B3k4+SL5EfkfspuhRbih8lliKkzKQsp2ynNFEuULoo/VQ9qj01gJpIzaEuoFZS66gnqbepL7W0tKy0fLUmaEm05mtVau3TOq11T+sdTZ/mROPS0mlK2jLaTloz7SbtJZ1Ot6MH09PoBfRl9Br6Cfpd+ltthvZobZ62UHuedpV2vfYl7Wc6FB1bHY7OFJ1inQqdAzoXdHp0Kbp2ulxdvu5c3SrdQ7rXdfv0GHpuerF6+XpL9XbrndF7rE/St9MP0xfqL9Lfpn9Cv5OBMawZXIaAsZCxnXGS0WVANLA34BnkGJQb/GjQZtBrqG/oaZhsWGRYZXjEsIOJMe2YPGYeczlzP/Ma8/0IsxGcEaIRS0bUjbg04o3RSKNgI5FRmdFeo6tG741ZxmHGucYrjRuM75jgJk4mE0xmmGw2OWnSM9JgpP9IwciykftH/m6KmjqZxpvOMt1met60z8zcLMJMZrbe7IRZjznTPNg8x3yN+VHzbguGRaCFxGKNxTGLJyxDFoeVx6pktbJ6LU0tIy2Vllst2yz7reytkqxKrPZa3bGmWrOts6zXWLdY99pY2Iy3mW1Ta/O7LcWWbSu2XWd7yvaNnb1dit23dg12j+2N7Hn2xfa19rcd6A5BDtMdqh2uOBId2Y65jpscLzqhTl5OYqcqpwvOqLO3s8R5k3P7KMIo31HSUdWjrrvQXDguhS61LvdGM0dHjy4Z3TD62RibMWljVo45NeaTq5drnut211tu+m7j3ErcmtxeuDu5C9yr3K940D3CPeZ5NHo893T2FHlu9rzhxfAa7/WtV4vXR28fb7l3nXe3j41Phs9Gn+tsA3Yceyn7tC/BN8R3nu9h33d+3n4Ffvv9/vJ38c/13+3/eKz9WNHY7WM7A6wC+AFbAzoCWYEZgd8HdgRZBvGDqoPuB1sHC4N3BD/iOHJyOHs4z0JcQ+QhB0PecP24c7jNoVhoRGhZaFuYflhS2Iawu+FW4dnhteG9EV4RsyKaIwmRUZErI6/zzHgCXg2vd5zPuDnjWqNoUQlRG6LuRztFy6ObxqPjx41fPf52jG2MNKYhFsTyYlfH3omzj5se98sE4oS4CVUTHsa7xc+OP5XASJiasDvhdWJI4vLEW0kOScqklmSd5PTkmuQ3KaEpq1I6Jo6ZOGfiuVSTVElqYxopLTltR1rfpLBJayd1pXull6Zfm2w/uWjymSkmU/KmHJmqM5U/9UAGISMlY3fGB34sv5rfl8nL3JjZK+AK1gmeCoOFa4TdogDRKtGjrICsVVmPswOyV2d3i4PEFeIeCVeyQfI8JzJnS86b3NjcnbkDeSl5e/PJ+Rn5h6T60lxp6zTzaUXT2mXOslJZx3S/6Wun98qj5DsUiGKyorHAAB7ezysdlN8o7xUGFlYVvp2RPONAkV6RtOj8TKeZS2Y+Kg4v/mEWPkswq2W25ewFs+/N4czZOheZmzm3ZZ71vEXzuuZHzN+1gLogd8FvJa4lq0peLUxZ2LTIbNH8RZ3fRHxTW6pdKi+9/q3/t1sW44sli9uWeCxZv+RTmbDsbLlreUX5h6WCpWe/c/uu8ruBZVnL2pZ7L9+8grhCuuLayqCVu1bprSpe1bl6/Or6Naw1ZWterZ269kyFZ8WWddR1ynUdldGVjett1q9Y/2GDeMPVqpCqvRtNNy7Z+GaTcNOlzcGb67aYbSnf8v57yfc3tkZsra+2q67YRtxWuO3h9uTtp35g/1Czw2RH+Y6PO6U7O3bF72qt8amp2W26e3ktWqus7d6Tvufij6E/Nta51G3dy9xbvg/sU+578lPGT9f2R+1vOcA+UPez7c8bDzIOltUj9TPrexvEDR2NqY3th8Ydamnybzr4y+hfdh62PFx1xPDI8qPUo4uODhwrPtbXLGvuOZ59vLNlasutExNPXGmd0Np2Murk6V/Dfz1xinPq2OmA04fP+J05dJZ9tuGc97n6817nD/7m9dvBNu+2+gs+Fxov+l5sah/bfvRS0KXjl0Mv/3qFd+Xc1Zir7deSrt24nn6944bwxuObeTef/174e/+t+bcJt8vu6N6puGt6t/oPxz/2dnh3HLkXeu/8/YT7tzoFnU8fKB586Fr0kP6w4pHFo5rH7o8Pd4d3X3wy6UnXU9nT/p7SP/X+3PjM4dnPfwX/db53Ym/Xc/nzgRdLXxq/3PnK81VLX1zf3df5r/vflL01frvrHfvdqfcp7x/1z/hA+lD50fFj06eoT7cH8gcGZHw5f/AogMGKZmUB8GInAPRUABgX4flhkvrON1gQ9T11kMB/YvW9cLB4A1AHG9VxndsMwD5Y7WClzwdAdVRPDAaoh8dw1RRFloe72hYN3ngIbwcGXpoBQGoC4KN8YKB/08DAR3hHxW4C0DxdfddUFSK8G3wfoKKrRsL54Kuivod+EePXLVB54Am+bv8F0nSIcoyVGVcAAABsZVhJZk1NACoAAAAIAAQBGgAFAAAAAQAAAD4BGwAFAAAAAQAAAEYBKAADAAAAAQACAACHaQAEAAAAAQAAAE4AAAAAAAAAkAAAAAEAAACQAAAAAQACoAIABAAAAAEAAAA0oAMABAAAAAEAAAAyAAAAALjV8aUAAAAJcEhZcwAAFiUAABYlAUlSJPAAAASiSURBVFgJ7Zhrd5pIHMajgoCoeEk0BlDTXHq6zfb7f4y227zYtGka4x2rolEhXOxDJk1cDJEg9uTsybzwMCMz8+OZ/w0i8/l866W26EsFc7he4YKezotWjgr6VI/Pg3uhmYahz2a4g+E4iqYjt+3xCU+Ohg9nmtb1aNytX2HfgiQnBYGiYsB7EuPxP0ODI5rNNH04HCntVuPiAhtq88iOYWUyaY5lAugXJhw0A9m/X89rl5dKqwk4ZaKXx9O3Rwf0di6AfiHALWpWu4JkP1qtlj6dAE5vNC17zrEsrgPoFw7cvWYgazTqhqZl+SSABpNrdHEx07QA+q0Ft6xZp93eMm9yqaQsStFYjOkp4+lMURTwQT84RTYrcJwjpJ+2LpxLM5CV8hlZFPePjuNcQh4MruqNs7NvRD9d19+/O9443LJm9Xp9pA5YKmZbNmIbn06ls7l4gsOBfj07U4cDFBgsy1YrUt7O+vTcgMoBzqUZyAzDsE2z1u5GGC4vyrE4a1n2bYSL4C/coA772kzDRJ+e+2y4Zc1gUnGa3tnZhhnh4EbqqK30Ln5cTqZOklC6CgZpihLSaSGdouln7BjBZn5s8/4e24Y6ltLrI54R3wTZ8fFhPp93UBTl9PQUfIIgMAyDEZCpqoruycn7w8MDWRIFIR3+sXpptlvc2a9WirtFoCQTXL/buZlOB/2+YZoYASLIKpXywcEbWZZ4PhGN+q02niHysp0RzUAminuplBPYbF17I5XmugbLU68dOKIZyPb2SiCjYjEM+my+4J7WrFyWU0meoijTsracBH+X4xc1wz0gw8P4xCK3+YVz+eZ/NPtNNplMlZ+D7/UWZJtqejaXg50F08wX3LJmdzkgn6nI0qJmqjpqNltwUriqZpggI3YWTDO/cC7NSA7Yl0rFbWSpu9OEZiD7+PHT5WXt3jfX0WwF3LJmqDWQ0W/zpliWZVg6sTOi2fn593uy9TVbDefSjNQayOjImyVJ5HgeHrAhzVbA2fb85saAKvV6o9lqIe2kEpwkitVqtbBb4tNpkG1OsxVwlmUhZ4/Go0G/N5tMoFNJFN+e/FWWRT6V3LRmK+DsuW2YBpLPbDbFD8OyFE2h1uBSvL01H6tjeMAm7IxgrYBbvInUFP2fveFwSDJBu90O3TcXd3wGnG1ZumUNB4NaraZpGmZ2u93QfTMgHJmGAPblyynDfEN3sdZYP54tY5ERz/QVQZp02u9MeQsEwci09fOmF9DiuCccoO7oHvAeJq5TazyssurKEy4ajTLxeEYQymWJZZ2ycbEVCjtHR4eSJAaoNRbXefrasxJ2Kl6EuulMHam6prtWYVhGSAtcgkN9hsdw/RtW1xOO5FbkCURjxLyw9nOtE43g7RaPRyzI/bHH81gBh9yKDIY8gWjsWjSsLk3ReNmOx/EC9MiXKE84kluHqtrpdq+vnQ8fm2jJJF8sFGDZsI1l6/CEI7kVZJ8//9PpdDZBhjWLxeKHD38jMOGVEeK5dtmULbu2Cdb1dAgYnIYvgX/qWBGtlpXzhCMvz3/SIZaNzhPuJYQST7hgVhLurBftEK9wQQ/7Vbn/pXK/AE4OtgYO+7i0AAAAAElFTkSuQmCC">
                    <span>修改 OKR</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="bottom-btn">
              <span class="confirm-submit">发布</span>
              <span class="cancel-submit">取消</span>
            </div>
            <div class="bottom-remind-info">
<!--              <img src="./images/schedule.png"/>-->
              <span>进度记录：保持更新，让小伙伴们看到你的进度</span>
            </div>
            <div class="current-item-id">${item.id}</div>
          </div>
            `
          item.kr.forEach((el, idx) => {
            htmr +=
                `<li>
                <p class="left">
                  <span>KR${idx + 1}</span>
                  <input disabled="true" class="keyInput" type="text" placeholder="请输入Key Result的值" value="${el.content}">
                </p>
                <p class="right">
                  <input disabled="true" class="progress-val" value="${el.progress}%"></input>
                  <input disabled="true" class="scores" value="${el.score}"></input>
                  <input disabled="true" class="weights" value="${el.weight}%"></input>
                  <span class="delete">×</span>
                </p>
              </li > 
              `
          })
          $('#objective-box').append(htm);
          if (item.alignment_content) {
            $($('.alignment-content')[index]).removeClass('alignment-content-hidden');
          }
          $($($('#objective-box').find('.objective-item')[index]).find('ul')[0]).append(htmr);
        })

        // 获取所有的.add-key .add标签
        addKeyList = document.querySelectorAll('.add-key .add');
        // 获取所有的.objective-item标签
        addKeyParentList = document.querySelectorAll('.objective-item');
      } else {
        remindInfoBox('请求失败！', 2000);
      }
    }
  })
}

// 13.2点击修改OKR
$(document).on("click", '.update', function (el) {
  // 拿到父元素
  const objectiveItem = $(el.target).parents('.objective-item')[0];
  // 移除特定样式类名
  $(objectiveItem).removeClass('allow-change-info');
  // 允许数据可以重新编辑
  const inputList = $(objectiveItem).find('input');

  $(inputList).each((index, item) => $(item).removeAttr("disabled"));
  return
})

// 14.添加或者修改 Objective
function addOrUpdateObjective(Objective) {
  $.ajax({
    type: 'post',
    url: createLink('okr', 'ajax_upsert_objective'),
    contentType: 'application/json',
    data: JSON.stringify(Objective),
    success: function (data) {
      if (data.code == 200) {
        // 给出删除成功提示
        if (Objective.id == 0) {
          remindSuccessInfoBox('添加成功！', 1500);
        } else {
          remindSuccessInfoBox('修改成功！', 1500);
        }
        setTimeout(() => {
          // 重新请求最新的数据
          getCurrentDateData(currentDateId.id);
          $(window).scrollTop(0);
          $(document).scrollTop(0);
        }, 1500)
      }
    }
  })
}

// 15.删除 Objective
function deleteCurrentObjective(val) {
  $.ajax({
    type: 'post',
    url: createLink('okr', 'ajax_delete_objective'),
    contentType: 'application/json',
    data: JSON.stringify({ objective_id: val }),
    success: function (data) {
      if (data.code == 200) {
        // 隐藏确认栏
        $('.remind-dialog-box').css('display', 'none');
        // 给出删除成功提示
        remindSuccessInfoBox('删除成功！', 1500);
        setTimeout(() => {
          // 重新请求最新的数据
          getCurrentDateData(currentDateId.id);
          $(window).scrollTop(0);
          $(document).scrollTop(0);
        }, 1500)
      } else {
        remindInfoBox('请求失败！', 2000);
      }
    }
  })
}

// 16.1 点击添加对齐(只有发布了的数据，也就是从服务器拿的数据，点击修改以后才可以添加对齐)
$(document).on("click", '.add-align .left .open-dialog-box', function (el) {
  // 还没点击修改okr前，不允许添加新的对齐
  // if ($($(el.target).parents('.objective-item')[0]).hasClass('allow-change-info')) return

  // 拿到当前objective的id
  const currentObjectId = $($($(el.target).parents('.objective-item')[0]).find('.current-item-id')).html();
  if (currentObjectId) {
    // addAlignedObjective(currentDateId.id, currentObjectId)
    getAlignedObjective(el);
  }
})

// 16.2获取当前objective可以对齐的内容
let adminObjective;
function getAlignedObjective(el) {
  $.get(createLink('okr', 'ajax_alignment_objective'), function (data) {
    // console.log(data)
    if (data.code == 200) {
      // 获取到所有可以选择的对齐值
      adminObjective = data.data;
      // 拿到当前添加对齐内容的盒子
      const showAlignmentContentBox = $($(el.target).parents('.add-align .left')[0]).find('.show-alignment-content-box')[0];
      // 拿到所有添加内容的盒子
      const allShowAlignmentContentBox = $($(el.target).parents('#objective-box')[0]).find('.show-alignment-content-box');
      // 隐藏在其他地方点开的添加对齐内容
      $(allShowAlignmentContentBox).each((idx, item) => {
        $(item).css('visibility', 'hidden');
      })
      // 清空当前对齐盒子的内容，避免重复叠加
      $(showAlignmentContentBox).html('');
      // 定义变量接收参数
      adminObjective.forEach((item, index) => {
        let htm = '', htmr = '';
        htm =
            `
        <ul class="show-alignment-content">
          <li class="admin-name-title">
            <span>0${index + 1}</span>
            <span class="admin-name-msg">${item.admin}</span>
          </li>
        </ul>
        `
        item.contData.forEach((el, idx) => {
          htmr +=
              `
          <li class="admin-message-title">
            <span>0${idx + 1}</span>
            <span class="select-mes-cont">${el.content}</span>
            <span class="id-value">${el.id}</span>
          </li>
          `
        })
        // 先将所有的对齐名字放进去
        $(showAlignmentContentBox).append(htm);
        // 再将对应的内容放入对应的名字下面
        $($(showAlignmentContentBox).find('.show-alignment-content')[index]).append(htmr);
      })
      // 在最后加上一个不选的按钮
      $(showAlignmentContentBox).append('<li class="not-select">不选</li>');
      // 显示盒子
      $(showAlignmentContentBox).css('visibility', 'visible');
    } else {
      remindInfoBox('请求失败！', 2000);
    }
  })
}

// 16.3监听选择的对齐内容Li
// $(document).on("click", '.show-alignment-content li', function (el) {
//   // 拿到当前选择的元素的下标
//   let idx = $('.show-alignment-content li').index(this);
//   // 如果选择了不选，则不做处理
//   if (idx == $('.show-alignment-content li').length - 1) return;
//   // 如果不是不选，拿到当前选中的值的id
//   const currentId = adminObjective[idx].id;
//   // 拿到当前objective的id
//   const currentObjectId = $($($(el.target).parents('.objective-item')[0]).find('.current-item-id')).html();
//   // 拿到选中的值
//   const currentVal = $($('.show-alignment-content li')[idx]).html();
//   // 拿到显示该值的标签
//   const htmlt = $($(el.target).parents('.add-align .left')[0]).find('.alignment-content')[0];

//   // 调用函数
//   addAlignedObjective(currentId, currentObjectId, currentVal, htmlt);
// })
// 16.3监听选择的对齐内容Li
$(document).on("click", '.show-alignment-content-box .admin-message-title span', function (el) {
  // 拿到当前选中的值的id
  const currentId = $($($(el.target).parents('.admin-message-title')[0]).find('.id-value')).html();
  // 拿到当前objective的id
  const currentObjectId = $($($(el.target).parents('.objective-item')[0]).find('.current-item-id')).html();
  // 拿到选中的值
  const currentVal = $($($(el.target).parents('.admin-message-title')[0]).find('.select-mes-cont')).html();
  // 拿到显示该值的标签
  const htmlt = $($(el.target).parents('.add-align .left')[0]).find('.alignment-content')[0];
  // 拿到当前显示筛选对齐的盒子，方便等下隐藏
  const hiddenBox = $($(el.target).parents('.show-alignment-content-box')[0]);
  // 拿到当前对齐的小标题（人物名字）
  const adminName = $($($(el.target).parents('.show-alignment-content')[0]).find('.admin-name-msg')).html();
  // 调用函数

  // $($(htmlt).find('.alignment-content'))
  addAlignedObjective(currentId, currentObjectId, currentVal, htmlt, hiddenBox, adminName);
})

// 16.5点击不选的时候关闭当前弹窗
$(document).on("click", '.show-alignment-content-box .not-select', function (el) {
  // 拿到当前盒子
  const showAlignmentContentBox = $(el.target).parents('.show-alignment-content-box')[0];
  // 隐藏当前盒子
  $(showAlignmentContentBox).css('visibility', 'hidden');
})

// 16.4添加当前objective的对齐内容
function addAlignedObjective(currentId, currentObjectId, currentVal, htmlt, hiddenBox, adminName) {
  $.ajax({
    type: 'post',
    url: createLink('okr', 'ajax_alignment_objective'),
    contentType: 'application/json',
    data: JSON.stringify({ objective_id: currentObjectId, alignment_objective_id: currentId }),
    success: function (data) {
      // console.log(data)
      if (data.code == 200) {
        remindSuccessInfoBox('添加成功！', 1000);
        $(htmlt).removeClass('alignment-content-hidden');
        $($(htmlt).find('.cont-style')[0]).html(adminName + '：');
        $($(htmlt).find('.cont-mes')[0]).html(currentVal);
        // $(htmlt).html(currentVal);
        $(hiddenBox).css('visibility', 'hidden');
      } else {
        remindInfoBox('请求失败！', 2000);
      }
    }
  })
}
