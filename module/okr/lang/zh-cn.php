<?php
/**
 * The doc module zh-cn file of ZenTaoPMS.
 *
 * @copyright   Copyright 2009-2015 青岛易软天创网络科技有限公司(QingDao Nature Easy Soft Network Technology Co,LTD, www.cnezsoft.com)
 * @license     ZPL (http://zpl.pub/page/zplv12.html)
 * @author      Chunsheng Wang <chunsheng@cnezsoft.com>
 * @package     doc
 * @version     $Id: zh-cn.php 824 2010-05-02 15:32:06Z wwccss $
 * @link        http://www.zentao.net
 */
/* 字段列表。*/
$lang->okr->common    = 'OKR';
$lang->okr->api       = 'API';
$lang->okr->index     = '首页';
$lang->okr->newMethod = '新增方法';
$lang->okr->add    = '新增';
$lang->okr->create    = '新增';
$lang->okr->newLang   = '新增语言';
$lang->okr->newConfig = '新增配置';
$lang->okr->newHook   = '新增钩子';
$lang->okr->newExtend = '新增扩展';
$lang->okr->newPage   = '新增页面';
$lang->okr->override  = '覆盖';
$lang->okr->edit      = '编辑扩展';
$lang->okr->save      = '保存页面';
$lang->okr->delete    = '删除页面';
$lang->okr->period    = '周期';
$lang->okr->index    = '首页';
$lang->okr->id    = 'ID';
$lang->okr->title    = '标题';
$lang->okr->startAt    = '开始时间';
$lang->okr->begin    = '开始时间';
$lang->okr->createdAt    = '创建时间';
$lang->okr->to    = '到';
$lang->okr->updatedAt    = '修改时间';
$lang->okr->endAt    = '结束时间';
$lang->okr->end    = '结束时间';
$lang->okr->progress    = '进度';
$lang->okr->weight    = '权重';
$lang->okr->score    = '分数';
$lang->okr->objective    = 'objective';
$lang->okr->kr    = 'key result';

$lang->okr->api_get_period  = 'api获取okr周期列表';
$lang->okr->api_get_period_data  = 'api获取okr周期数据';
$lang->okr->api_upsert_objective  = 'api修改objective';
$lang->okr->api_alignment_objective  = 'api对齐objective';
$lang->okr->createPeriod  = '创建周期';
$lang->okr->deletePeriod  = '删除周期';
$lang->okr->setDefaultPeriod  = '设为默认';