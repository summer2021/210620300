<?php
/**
 * The doc module zh-cn file of ZenTaoPMS.
 *
 * @copyright   Copyright 2009-2015 青岛易软天创网络科技有限公司(QingDao Nature Easy Soft Network Technology Co,LTD, www.cnezsoft.com)
 * @license     ZPL (http://zpl.pub/page/zplv12.html)
 * @author      Chunsheng Wang <chunsheng@cnezsoft.com>
 * @package     doc
 * @version     $Id: zh-cn.php 824 2010-05-02 15:32:06Z wwccss $
 * @link        http://www.zentao.net
 */
/* 字段列表。*/
$lang->okr->common    = 'common';
$lang->okr->api       = 'api';
$lang->okr->index     = 'index';
$lang->okr->newMethod = 'new method';
$lang->okr->add    = 'add';
$lang->okr->create    = 'create';
$lang->okr->newLang   = 'new lang';
$lang->okr->newConfig = 'new config';
$lang->okr->newPage   = 'new page';
$lang->okr->override  = 'override';
$lang->okr->edit      = 'edit';
$lang->okr->save      = 'save';
$lang->okr->delete    = 'delete';
$lang->okr->period    = 'period';
$lang->okr->index    = 'index';
$lang->okr->id    = 'ID';
$lang->okr->title    = 'title';
$lang->okr->startAt    = 'startAt';
$lang->okr->begin    = 'begin';
$lang->okr->createdAt    = 'createdAt';
$lang->okr->to    = 'to';
$lang->okr->updatedAt    = 'updatedAt';
$lang->okr->endAt    = 'endAt';
$lang->okr->end    = 'end';
$lang->okr->progress    = 'progress';
$lang->okr->weight    = 'weight';
$lang->okr->score    = 'score';
$lang->okr->objective    = 'objective';
$lang->okr->kr    = 'key result';

$lang->okr->api_get_period  = 'api get period';
$lang->okr->api_get_period_data  = 'api get period data';
//$lang->okr->api_upsert_objective  = 'api修改objective';
//$lang->okr->api_alignment_objective  = 'api对齐objective';
$lang->okr->createPeriod  = 'create Period';
$lang->okr->deletePeriod  = 'delete Period';
$lang->okr->setDefaultPeriod  = 'set Default';