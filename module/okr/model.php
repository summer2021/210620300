<?php
/**
 * The model file of doc module of ZenTaoPMS.
 *
 * @copyright   Copyright 2009-2015 青岛易软天创网络科技有限公司(QingDao Nature Easy Soft Network Technology Co,LTD, www.cnezsoft.com)
 * @license     ZPL (http://zpl.pub/page/zplv12.html)
 * @author      Chunsheng Wang <chunsheng@cnezsoft.com>
 * @package     doc
 * @version     $Id: model.php 881 2010-06-22 06:50:32Z chencongzhi520 $
 * @link        http://www.zentao.net
 */
?>
<?php
class okrModel extends model
{
    public function getOKR()
    {
        $okrs = $this->dao->select('*')->from('zt_okr_objective')
            ->Where('deleted')->eq(0)
            ->query();
        $okrs = $okrs->fetchAll();
        return $okrs;
    }
    public function getAllPeriod()
    {
        $okrs = $this->dao->select('*')
            ->from('zt_okr_period')
            ->where('deleted')->eq('0')
            ->orderBy('isDefault_desc')
            ->query();
        $okrs = $okrs->fetchAll();
        return $okrs;
    }

    public function getPeriodList($orderBy = 'id_desc', $pager = null)
    {
        return $this->dao->select('*')->from('zt_okr_period')
            ->where('deleted')->eq('0')
            ->andwhere('createdBy')->eq($this->app->user->account)
            ->andWhere('deleted')->eq(0)
            ->orderBy($orderBy)
            ->page($pager)
            ->fetchAll('id');
    }

    /**
     * Create a createPeriod.
     *
     * @access public
     * @return void
     */
    public function createPeriod()
    {
        $period = fixer::input('post')
            ->add('createdBy', $this->app->user->account)
            ->add('createdDate', date('Y-m-d H:i:s'))
            ->add('lastEditedBy', $this->app->user->account)
            ->add('lastEditedDate',date('Y-m-d H:i:s'))
            ->skipSpecial('url,token,account,password')
            ->get();

        $this->dao->insert('zt_okr_period')->data($period)
            ->batchCheck('name,begin,end', 'notempty')
            ->exec();
        if(dao::isError()) return false;
        return $this->dao->lastInsertId();
    }

    public function getOkrByPeriodId($id)
    {
        $objectives = $this->dao->select('*')->from('zt_okr_objective')
            ->where('period')->eq((int)$id)
            ->andWhere('owner')->eq($this->app->user->account)
            ->andWhere('deleted')->eq(0)
            ->orderBy('createdDate asc')
            ->fetchAll();
        if(!$objectives) return [];
        foreach($objectives as $k=>$objective){
            $kr = $this->dao->select('*')->from('zt_okr_key_result')
                ->where('objective')->eq($objective->id)
                ->orderBy('sort asc')
                ->fetchAll();
            $objectives[$k]->progress=$objective->progress/100;
            $objectives[$k]->weight=$objective->weight/100;
            $objectives[$k]->score=$objective->score/100;
            foreach ($kr as $key => $value) {
                $kr[$key]->progress=$value->progress/100;
                $kr[$key]->weight=$value->weight/100;
                $kr[$key]->score=$value->score/100;
            }
            $objectives[$k]->kr=$kr;
            $alignmentContent="";
            $alignmentOwner="";
            if($objective->alignment>0){
                $alignment=$this->dao->select('*')->from('zt_okr_objective')
                    ->where('id')->eq($objective->alignment)
                    ->fetch();
                $alignmentContent=$alignment->content;
                $alignmentOwner=$alignment->owner;
            }
            $objectives[$k]->alignment_content=$alignmentContent;
            $objectives[$k]->alignment_owner=$alignmentOwner;
        }
        return $objectives;
    }



}
