<?php include '../../common/view/header.html.php'; ?>
<div class="nav-title">
    <div class="left-name">
        <?php
        echo (empty($app->user->realname) ? $app->user->account : $app->user->realname);
        ?></div>
    <div class="right-date">
    </div>
</div>

<div id="objective-box"></div>

<div class="add-objective">
    <span>+</span>
    <span>添加&nbsp;Objective</span>
</div>

<div class="remind-dialog-box">
    <div class="remind-dialog">
        <div class="title">对话框确认</div>
        <div class="content">您确认删除当前Objective吗？</div>
        <div class="btn">
            <span class="remind-cancel">取消</span>
            <span class="remind-confirm">确认</span>
        </div>
    </div>
</div>

<div class="remind-info-dialog-box">
    <div class="remind-info-dialog">
        你的信息未填完整 ！
    </div>
</div>

<div class="remind-info-success-dialog-box">
    <div class="remind-info-success-dialog">
        操作成功 ！
    </div>
</div>
<?php include '../../common/view/footer.html.php'; ?>
