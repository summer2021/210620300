<?php include '../../common/view/header.html.php'; ?>

<div id="mainMenu" class="clearfix">
    <div class="btn-toolbar pull-right">
        <?php common::printLink('okr', 'createPeriod', "", "<i class='icon icon-plus'></i> " . $lang->okr->add, '', "class='btn btn-primary'");?>
    </div>
</div>

<div id='mainContent' class='main-row'>
    <form class='main-table' id='ajaxForm' method='post'>
        <table id='jenkinsList' class='table has-sort-head table-fixed'>
            <thead>
            <tr>
                <?php $vars = "orderBy=%s&recTotal={$pager->recTotal}&recPerPage={$pager->recPerPage}&pageID={$pager->pageID}"; ?>
                <th class='w-60px'><?php common::printOrderLink('id', $orderBy, $vars, $lang->okr->id); ?></th>
                <th class='text-left'><?php common::printOrderLink('name', $orderBy, $vars, $lang->okr->title); ?></th>
                <th class='text-left'><?php common::printOrderLink('begin', $orderBy, $vars, $lang->okr->begin); ?></th>
                <th class='text-left'><?php common::printOrderLink('end', $orderBy, $vars, $lang->okr->end); ?></th>
                <th class='text-left'><?php echo $lang->okr->setDefaultPeriod; ?></th>
                <th class='w-100px c-actions-4'><?php echo $lang->actions; ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($periodList as $id => $period): ?>
                <tr>
                    <td class='text-center'><?php echo $id; ?></td>
                    <td class='text' title='<?php echo $period->name; ?>'><?php echo $period->name; ?></td>
                    <td class='text' title='<?php echo $period->begin; ?>'><?php echo $period->begin; ?></td>
                    <td class='text' title='<?php echo $period->end; ?>'><?php echo $period->end; ?></td>
                    <td class='text' title='<?php echo $period->isDefault; ?>'><?php echo $period->isDefault?'默认':'非默认'; ?></td>
                    <td class='c-actions text-left'>
                        <?php
                        if(common::hasPriv('okr', 'setDefaultPeriod')) echo html::a($this->createLink('okr', 'setDefaultPeriod', "id=$id"), '<i class="icon icon-exchange"></i>', 'hiddenwin', "title='{$lang->okr->setDefaultPeriod}' class='btn'");
                        if(common::hasPriv('okr', 'deletePeriod')) echo html::a($this->createLink('okr', 'deletePeriod', "okrId=$id"), '<i class="icon-trash"></i>', 'hiddenwin', "title='{$lang->okr->delete}' class='btn'");
                        ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <?php if($periodList):?>
            <div class='table-footer'><?php $pager->show('right', 'pagerjs');?></div>
        <?php endif; ?>
    </form>
</div>
<?php include '../../common/view/footer.html.php'; ?>

