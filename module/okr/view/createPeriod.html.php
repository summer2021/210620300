<?php

?>
<?php include '../../common/view/header.html.php'; ?>
<div id='mainContent' class='main-row'>
    <div class='main-col main-content'>
        <div class='center-block'>
            <div class='main-header'>
                <h2><?php echo $lang->okr->create;?></h2>
            </div>
            <form id='jenkinsForm' method='post' class='form-ajax'>
                <table class='table table-form'>
                    <tr>
                        <th><?php echo $lang->okr->title; ?></th>
                        <td class='required'><?php echo html::input('name', '', "class='form-control'"); ?></td>
                        <td></td>
                    </tr>
                  <tr>
                      <th><?php echo $lang->okr->period;?></th>
                      <td>
                          <div class='input-group'>
                              <?php echo html::input('begin', '', "class='form-control form-date' placeholder='" . $lang->okr->begin . "' required");?>
                              <span class='input-group-addon'><?php echo $lang->okr->to;?></span>
                              <?php echo html::input('end','', "class='form-control form-date' placeholder='" . $lang->okr->end . "' required");?>
                          </div>
                      </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td class='text-center form-actions'>
                            <?php echo html::submitButton(); ?>
                            <?php echo html::backButton(); ?>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>
<?php include '../../common/view/footer.html.php'; ?>

