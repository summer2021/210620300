CREATE TABLE `zt_okr_key_result` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `objective` bigint NOT NULL COMMENT '所属Objective',
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'KR内容',
  `progress` mediumint NOT NULL DEFAULT '0' COMMENT '进度',
  `weight` mediumint NOT NULL DEFAULT '0' COMMENT '权重',
  `score` mediumint NOT NULL DEFAULT '0' COMMENT '分数',
  `sort` mediumint NOT NULL,
  `createdDate` datetime NOT NULL,
  `lastEditedDate` datetime NOT NULL,
  `deleted` tinyint NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `objective_id_index` (`objective`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


CREATE TABLE `zt_okr_objective` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `content` mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Objective内容',
  `alignment` bigint NOT NULL DEFAULT '0' COMMENT '对齐的上级Objective',
  `period` int NOT NULL COMMENT '所属的周期',
  `progress` mediumint NOT NULL DEFAULT '0' COMMENT '进度',
  `weight` mediumint NOT NULL COMMENT '权重',
  `score` mediumint NOT NULL DEFAULT '0' COMMENT '分数',
  `order` mediumint NOT NULL COMMENT '排序',
  `owner` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '拥有者',
  `createdDate` datetime NOT NULL,
  `lastEditedDate` datetime NOT NULL,
  `deleted` tinyint NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `alignment_objective_id_index` (`alignment`),
  KEY `period_id_index` (`period`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


CREATE TABLE `zt_okr_period` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '周期名称',
  `createdBy` char(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '创建者',
  `begin` date DEFAULT NULL COMMENT '开始日期',
  `end` date DEFAULT NULL COMMENT '结束日期',
  `createdDate` datetime NOT NULL,
  `lastEditedBy` char(30) NOT NULL,
  `lastEditedDate` datetime NOT NULL,
  `deleted` tinyint NOT NULL DEFAULT '0',
  `isDefault` tinyint NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


